#!/bin/bash

declare -a Hosts=(
    "51.144.126.116"
    "137.117.147.116"
    "137.117.164.59"
    "51.136.57.161"
    "137.135.130.43"
    "104.40.142.56"
)


echo "Running Rancher Clean Up on all hosts."

for host in ${Hosts[@]}; do
    echo "---------- Running on $host ..."
    ssh "eatoo-admin@$host" "sudo bash -s" < cleanup_node.sh
done
