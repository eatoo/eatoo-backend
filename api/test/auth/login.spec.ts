import { expect, assert } from 'chai';
import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import { ValidationPipe } from '@nestjs/common';

export default (urls, users) => {
  let app;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  /**
   * Should login as toto with only username
   */
  it('Should login as toto with only username', (done) => {
    request(app.getHttpServer())
      .post(urls.login)
      .send({
        username: users.toto.username,
        password: users.toto.password,
      })
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body).to.have.property('token');
        expect(res.body).to.have.property('user');
        expect(res.body.user).to.have.property('email').to.equal(users.toto.email);
        expect(res.body.user).to.have.property('username').to.equal(users.toto.username);
        done();
      })
    ;
  });

  /**
   * Should login as toto with only email
   */
  it('Should login as toto with only email', (done) => {
    request(app.getHttpServer())
      .post(urls.login)
      .send({
        email: users.toto.email,
        password: users.toto.password,
      })
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body).to.have.property('token');
        expect(res.body).to.have.property('user');
        expect(res.body.user).to.have.property('email').to.equal(users.toto.email);
        expect(res.body.user).to.have.property('username').to.equal(users.toto.username);
        done();
      })
    ;
  });

  /**
   * Should fail for login with incorrect password
   */
  it('Should fail for login with incorrect password', (done) => {
    request(app.getHttpServer())
      .post(urls.login)
      .send({
        email: users.toto.email,
        password: users.toto.password + 'a',
      })
      .end((err, res) => {
        expect(res.status).to.equal(401);
        done();
      })
    ;
  });

  /**
   * Should fail for login with too few fields
   */
  it('Should fail for login with too few fields', (done) => {
    request(app.getHttpServer())
      .post(urls.login)
      .send({ email: users.toto.email })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      })
    ;
  });

  /**
   * Should fail for login with too few fields bis
   */
  it('Should fail for login with too few fields bis', (done) => {
    request(app.getHttpServer())
      .post(urls.login)
      .send({ password: users.toto.password })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      })
    ;
  });

  afterAll(async () => {
    await app.close();
  });
};
