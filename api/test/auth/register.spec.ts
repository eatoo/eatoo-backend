import { expect, assert } from 'chai';
import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import { ValidationPipe } from '@nestjs/common';

export default (urls, users) => {
  let app;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  /**
  * Should register a new user : toto
  */
  it('Should register a new user : toto', (done) => {
    request(app.getHttpServer())
    .post(urls.register)
    .send(users.toto)
    .end((err, res) => {
      expect(res.status).to.equal(201);
      expect(res.body).to.have.property('user');
      expect(res.body.user).to.have.property('email').to.equal(users.toto.email);
      expect(res.body.user).to.have.property('username').to.equal(users.toto.username);
      expect(res.body.user).to.have.property('phone').to.equal(users.toto.phone);
      done();
    })
    ;
  });

  /**
  * Should fail for register toto (Create twice)
  */
  it('Should fail for register toto (Create twice)', (done) => {
    request(app.getHttpServer())
    .post(urls.register)
    .send(users.toto)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should fail because username is empty
  */
  it('Should fail because username is empty', (done) => {
    request(app.getHttpServer())
    .post(urls.register)
    .send({
      email: users.toto.email,
      password: users.toto.password,
      phone: users.toto.phone
    })
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should fail because email is empty
  */
  it('Should fail because email is empty', (done) => {
    request(app.getHttpServer())
    .post(urls.register)
    .send({
      username: users.toto.username,
      password: users.toto.password,
      phone: users.toto.phone
    })
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should fail because password is empty
  */
  it('Should fail because password is empty', (done) => {
    request(app.getHttpServer())
    .post(urls.register)
    .send({
      username: users.toto.username,
      email: users.toto.email,
      phone: users.toto.phone
    })
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should fail because phone is empty
  */
  it('Should fail because phone is empty', (done) => {
    request(app.getHttpServer())
    .post(urls.register)
    .send({
      username: users.toto.username,
      email: users.toto.email,
      password: users.toto.password,
    })
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should fail because phone contains letter(s)
  */
  it('Should fail because phone contains letter(s)', (done) => {
    request(app.getHttpServer())
    .post(urls.register)
    .send({
      username: users.toto.username,
      email: users.toto.email,
      password: users.toto.password,
      phone: "06122142NO"
    })
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should fail because phone is not equal to 10 numbers
  */
  it('Should fail because phone is not equal to 10 numbers', (done) => {
    request(app.getHttpServer())
    .post(urls.register)
    .send({
      username: users.toto.username,
      email: users.toto.email,
      password: users.toto.password,
      phone: "06"
    })
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  afterAll(async () => {
    await app.close();
  });
};
