import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import { ValidationPipe } from '@nestjs/common';

import RegisterTester from './auth/register.spec';
import LoginTester from './auth/login.spec';
import UpdateTester from './profile/update.spec';
import DishesTester from './dishes/dishes.spec';

const urls = {
  register: '/auth/register',
  login: '/auth/login',
  update: '/auth/me',
  dish: '/dish/create',
};

const genName = () => Math.random().toString(36).substring(2, 15) + 
Math.random().toString(36).substring(2, 15);
const genUsername = () => Buffer.from(genName()).toString('base64');

const userSeed = process.env.TEST_USER_SEED || genUsername();

const users = {
  toto: {
    username: `${userSeed}_toto`,
    email: `${userSeed}_toto@eatoo.com`,
    password: userSeed,
    phone: `0622222222`,
  },
  tata : {
    username: `${userSeed}_tata`,
    email: `${userSeed}_tata@eatoo.com`,
    password: userSeed,
    phone: `0611111111`,
  },
  tata1 : {
    email: `${userSeed}_tata1@eatoo.com`,
  },
};

const dishes = {
  dish: {
    name: "pasta",
    number: "2",
    price: "5",
    ingredients: ["poulet", "fromage"],
    hour: "10:55"
  },
};

describe('Authentication Tests(e2e)', () => {
  
  let app;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });
  
  describe('Register Tests(e2e)', () => RegisterTester(urls, users));
  
  describe('Login Tests(e2e)', () => LoginTester(urls, users));
  
  describe('Update Tests(e2e)', () => UpdateTester(urls, users));

  describe('Dishes Tests(e2e)', () => DishesTester(urls, dishes));
  
  afterAll(async () => {
    await app.close();
  });
  
});
