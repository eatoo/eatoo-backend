import { expect, assert } from 'chai';
import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import { ValidationPipe } from '@nestjs/common';

export default (urls, dishes) => {
  let app;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });
  
  /**
  * Should create a dish with valid datas
  */
  it('Should create a dish with valid datas', (done) => {
    request(app.getHttpServer())
    .post(urls.dish)   
    .send(dishes.dish)
    .end((err, res) => {
      expect(res.status).to.equal(201);
      done();
    });
  });

  /**
  * Should fail to create an uncomplete dish
  */
 const dishUncomplete = {
    name: "I'm an uncomplete dish",
  }
  it('Should fail to create an uncomplete dish', (done) => {
    request(app.getHttpServer())
    .post(urls.dish)   
    .send(dishUncomplete)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });

  /**
  * Should fail to create a dish if the number of parts contains letters
  */
 const dishNumber = {
    name: "pasta",
    number: "deux",
    price: "5",
    ingredients: ["poulet", "fromage"],
    hour: "10:55"
  }
  it('Should fail to create a dish if the number of parts contains letters', (done) => {
    request(app.getHttpServer())
    .post(urls.dish)   
    .send(dishNumber)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });

  /**
  * Should fail to create a dish if the price contains letters
  */
 const dishPrice = {
    name: "pasta",
    number: "2",
    price: "cinq",
    ingredients: ["poulet", "fromage"],
    hour: "10:55"
  }
  it('Should fail to create a dish if the price contains letters', (done) => {
    request(app.getHttpServer())
    .post(urls.dish)   
    .send(dishPrice)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });

  /**
  * Should fail to create a dish with hour's bad format
  */
 const dishHour = {
    name: "pasta",
    number: "2",
    price: "5",
    ingredients: ["poulet", "fromage"],
    hour: "10H55"
  }
  it('Should fail to create a dish with hour\'s bad format', (done) => {
    request(app.getHttpServer())
    .post(urls.dish)   
    .send(dishHour)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });

  /**
  * Should fail to create a dish with number in ingredient's list
  */
 const dishIngredients = {
  name: "pasta",
  number: "2",
  price: "5",
  ingredients: [2, "fromage"],
  hour: "10:55"
}
it('Should fail to create a dish with number in ingredient\'s list', (done) => {
  request(app.getHttpServer())
  .post(urls.dish)   
  .send(dishIngredients)
  .end((err, res) => {
    expect(res.status).to.equal(400);
    done();
  });
});
  
  afterAll(async () => {
    await app.close();
  });
};