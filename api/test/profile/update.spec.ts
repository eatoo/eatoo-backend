import { expect, assert } from 'chai';
import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import { ValidationPipe } from '@nestjs/common';

const genName = () => Math.random().toString(36).substring(2, 15) + 
Math.random().toString(36).substring(2, 15);
const genUsername = () => Buffer.from(genName()).toString('base64');

const userSeed = genUsername();

const auth = { 
  token: ''
}

export default (urls, users) => {
  let app;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    
    const res = await request(app.getHttpServer())
    .post(urls.register)
    .send(users.tata);      
    auth.token = res.body.token;
  });
  
  /**
  * Should modify the user email : tata with a valid one
  */
  it('Should modify the user email : tata with a valid one', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)
    .set('Authorization', 'Bearer ' + auth.token)
    .send(users.tata1)
    .end((err, res) => {
      expect(res.status).to.equal(200);
      expect(res.body).to.have.property('username');
      expect(res.body).to.have.property('email');
      done();
    });
  });
  
  /**
  * Should fail to modify the user email with an existing email
  */
  it('Should fail to modify the user email with an existing email', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)
    .set('Authorization', 'Bearer ' + auth.token)
    .send(users.toto)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      expect(res.body.message).to.match(/email/);
      done();
    });
  });
  
  /**
  * Should fail to modify email with a short email
  */
  var emailShort = {
    email : 'tooShort'
  }
  it('Should fail to modify email with a short email', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)   
    .set('Authorization', 'Bearer ' + auth.token)
    .send(emailShort)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should fail to modify email with a bad one
  */
  const emailFake = {
    email : 'errorepitech.com'
  }
  it('Should fail to modify email with a bad one', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)   
    .set('Authorization', 'Bearer ' + auth.token)
    .send(emailFake)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should modify the pseudo with a valid one
  */
  const newPseudo = {
    pseudo : `newPseudo_${userSeed}` 
  }
  it('Should modify the pseudo with a valid one', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)   
    .set('Authorization', 'Bearer ' + auth.token)
    .send(newPseudo)
    .end((err, res) => {
      expect(res.status).to.equal(200);
      done();
    });
  });
  
  /**
  * Should modify the phone number with a valide one
  */
  const newPhone = {
    phone : "0642424242" 
  }
  it('Should modify the phone number with a valide one', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)   
    .set('Authorization', 'Bearer ' + auth.token)
    .send(newPhone)
    .end((err, res) => {
      expect(res.status).to.equal(200);
      done();
    });
  });
  
  /**
  * Should fail to modify the phone number with a bad one (letter inside)
  */
  const phoneLetter = {
    phone : "06424242nn" 
  }
  it('Should fail to modify the phone number with a bad one (letter inside)', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)   
    .set('Authorization', 'Bearer ' + auth.token)
    .send(phoneLetter)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should fail to modify the phone number with a bad one (Too short)
  */
  const phoneShort = {
    phone : "06" 
  }
  it('Should fail to modify the phone number with a bad one (Too short)', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)   
    .set('Authorization', 'Bearer ' + auth.token)
    .send(phoneShort)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should modify the age with a valid one
  */
  const age = {
    age : "12" 
  }
  it('Should modify the age with a valid one', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)   
    .set('Authorization', 'Bearer ' + auth.token)
    .send(age)
    .end((err, res) => {
      expect(res.status).to.equal(200);
      done();
    });
  });
  
  /**
  * Should fail to modify the age with a bad one (letter inside)
  */
  const ageLetter = {
    age : "1n" 
  }
  it('Should fail to modify the age with a bad one (letter inside)', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)   
    .set('Authorization', 'Bearer ' + auth.token)
    .send(ageLetter)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should fail to modify the age with a bad one (Too Short)
  */
  const ageShort = {
    age : "1" 
  }
  it('Should fail to modify the age with a bad one (Too Short)', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)   
    .set('Authorization', 'Bearer ' + auth.token)
    .send(ageShort)
    .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
  
  /**
  * Should modify the dishes of the user
  */
  const dish = {
    name: "pasta",
    number: "2",
    price: "5",
    ingredients: "pastas, cream",
    hour: "10H55"
  }
  it('Should modify the dishes of the user', (done) => {
    request(app.getHttpServer())
    .patch(urls.update)   
    .set('Authorization', 'Bearer ' + auth.token)
    .send(dish)
    .end((err, res) => {
      expect(res.status).to.equal(200);
      done();
    });
  });
  
  afterAll(async () => {
    await app.close();
  });
};