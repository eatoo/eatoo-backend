'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('UsersOrderDishes', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
      },
      sellerId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        /*
        references: {
          model: 'Users',
          key: 'id',
          as: 'userId'
        }
        */
      },
      dishId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      buyerId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        /*
        references: {
          model: 'Users',
          key: 'id',
          as: 'createUserId'
        }
        */
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('UsersOrderDishes');
  }
};