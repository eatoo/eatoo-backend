module.exports = {
  ccmEnabled: false,
  database: {
    enabled: false,
    name: 'eatoo_test',
  },
  cache: {
    enabled: false,
  },
  microservices: {
    logger: {
      enabled: false,
    }
  }
};
