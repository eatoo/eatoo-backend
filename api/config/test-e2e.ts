module.exports = {
  database: {
    enabled: true,
    name: 'eatoo_test',
  },
  cache: {
    enabled: false,
  },
  microservices: {
    logger: {
      enabled: false,
    }
  }
};
