module.exports = {
  ccmEnabled: true,
  listenPort: 3000,
  database: {
    enabled: true,
    name: 'eatoo',
    url: 'mongodb+srv://eatoo:1hmR*q1x@eatoo-cluster-nndio.mongodb.net',
  },
  cache: {
    enabled: false,
    expireType: 'EX',
    expireTime: 60 * 60 * 24 * 7,
    server: {
      host: '127.0.0.1',
      port: 6379
    }
  },
  plugins: {
    jwt: {
      secret: 'hWCFaBxHGJ1y3eHZvaRAjOBPSOElWjvySWmIK8FPoHA=',
      expiresIn: 60 * 60 * 24 * 7,
    },
    swagger: {
      title: 'Eatoo API Documentation',
      path: 'doc',
    },
    rateLimit: {
      windowMs: 15 * 60 * 1000,
      max: 1000,
    },
  },
  tests: {
    newUserAdmin: false,
  },
  microservices: {
    logger: {
      host: '127.0.0.1',
      port: 6373,
      debugFails: false,
      
      /**
       * Possible values:
       * - false: Never retry to reconnect
       * - true: Always retry to reconnect
       * - <number>: Retry n times to reconnect
       */
       connectionRetries: false,
    
      /**
       * Enable the communication within the Logger Microservice
       */
      enabled: true,
    },
  },
  sequelize: {
    host: 'localhost',
    username: 'Maxime',
    password: 'password',
    database: 'Eatoo',
  }
};
