const sequelizeConfig = require('./config.json');
module.exports = {
  database: {
    name: 'eatoo',
  },
  cache: {
    enabled: false,
    server: {
      host: 'api-cache',
      port: 6379
    }
  },
  microservices: {
    logger: {
      host: 'logger',
      port: 6373,
      debugFails: false,
    },
  },
  sequelize: sequelizeConfig.production
};
