module.exports = {
  tests: {
    newUserAdmin: false,
  },
  microservices: {
    logger: {
      host: '127.0.0.1',
      port: 6373,
      debugFails: true,
      connectionRetries: true,
      enabled: true
    },
  },
  cache: {
    enabled: false,
    expireType: 'EX',
    expireTime: 60 * 60 * 24 * 7,
    server: {
      host: '127.0.0.1',
      port: 6379
    }
  },
  database: {
    host: "localhost",
    username: "eatoo-failllll",
    password: "password",
    db_name: "eatoo-backend",
  }
};
