import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { HelperService } from '../../helper/helper.service';
import { Observable, PartialObserver } from 'rxjs';
import { logger as c } from '../../constants/index';

@Injectable()
export class LoggerService {
  private readonly confConnRetries: any = false;
  private readonly enabled: boolean = true;
  private online: boolean = false;
  private connRetries: number = 0;
  
  constructor(
    @Inject(c.SERVICE_NAME) private readonly client: ClientProxy,
    private readonly helper: HelperService,
  ) {
    this.confConnRetries = this.helper.conf(c.CONF_KEYS.connectionRetries, false);
    this.enabled = this.helper.fromCcm(c.CONF_KEYS.enabled);
  }
  
  async connect(): Promise<boolean> {
    if (!this.enabled) {
      return false;
    }
    try {
      await this.client.connect();
      this.helper.verbose('Successful connection', c.LOG_CONTEXT, true);
      this.setOnline();
      return true;
    } catch (e) {
      this.helper.error(
        `Connection failed to Logger Microservice. (${e.code})`,
        'Trace: ' + e.message,
        c.LOG_CONTEXT,
        true,
      );
      this.setOnline(false);
      return false;
    }
  }
  
  private canRetryConnection(): boolean {
    if (!this.enabled || this.confConnRetries === false) {
      return false;
    }
    return this.confConnRetries === true || this.connRetries < this.confConnRetries;
  }
  
  setOnline(online: boolean = true) {
    this.online = online;
    if (!this.online) {
      const mode = (this.canRetryConnection()) ? 'RETRY' : 'OFFLINE';
      this.helper.warn(`Service entering ${mode} mode.`, c.LOG_CONTEXT);
    }
  }
  
  private handle<T>(caller: Observable<T>, observer: PartialObserver<T> = null): void {
    /**
     * observer should at least have the `complete` property.
     *
     */
    caller.subscribe(observer);
  }
  
  private _emitFail(e: any) {
    this.helper.error(
      `Cannot emit the Logger event. (${e.code})`,
      'Trace: ' + e.message,
      c.LOG_CONTEXT,
    );
    this.setOnline(false);
  }
  
  async emit<T>(
    pattern: string,
    data: any = '',
    observer: PartialObserver<T> = {
      complete: () => this.helper.verbose(`${pattern} sent.`, c.LOG_CONTEXT),
      error: (e: any) => this._emitFail(e),
    },
  ): Promise<any> {
    if (this.online) {
      this.handle<T>(this.client.emit<T>(pattern, data), observer);
      return true;
    } else if (this.canRetryConnection()) {
      if (typeof this.confConnRetries === 'number') {
        this.connRetries += 1;
        this.helper.warn(`Retrying to connect to Logger... (${this.connRetries})`);
      }
      if (await this.connect()) {
        this.handle<T>(this.client.emit<T>(pattern, data), observer);
        return true;
      }
    }
    if (this.helper.assertConf(c.CONF_KEYS.debugFails, true)) {
      this.helper.warn(`Event ${pattern} not emitted.`, c.LOG_CONTEXT);
    }
    return false;
  }
}
