import { Global, Logger, Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { LoggerService } from './logger.service';
import { conf, envOrConf } from '../../helper/helpers.methods';
import { logger as c } from '../../constants';

const imports = [];

@Global()
@Module({
  imports: [
    ClientsModule.register([
      {
        name: c.SERVICE_NAME,
        transport: Transport.TCP,
        options: {
          host: envOrConf(c.CONF_KEYS.envHost, c.CONF_KEYS.host, '127.0.0.1'),
          port: parseInt(envOrConf(c.CONF_KEYS.envPort, c.CONF_KEYS.port, 6373), 10),
        },
      },
    ]),
  ],
  providers: [LoggerService],
  exports: [LoggerService],
})
export class LoggerModule {
  constructor(private readonly loggerService: LoggerService) {}
  
  async onModuleInit() {
    await this.loggerService.connect();
  }
}
