import { Test, TestingModule } from '@nestjs/testing';
import { LoggerService } from './logger.service';
import { logger as c } from '../../constants';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { conf } from '../../helper/helpers.methods';
import { HelperService } from '../../helper/helper.service';
import { HelperModule } from '../../helper/helper.module';

describe('LoggerService', () => {
  let service: LoggerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ClientsModule.register([
          {
            name: c.SERVICE_NAME,
            transport: Transport.TCP,
            options: {
              host: conf(c.CONF_KEYS.host, '127.0.0.1'),
              port: conf(c.CONF_KEYS.port, 6373),
            },
          },
        ]),
      ],
      providers: [
        {
          provide: 'CCM',
          useValue: {}
        },
        LoggerService,
        HelperService
      ],
    }).compile();

    service = module.get<LoggerService>(LoggerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
