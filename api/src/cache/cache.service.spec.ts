import { Test, TestingModule } from '@nestjs/testing';
import { CacheService } from './cache.service';
import { HelperService } from '../helper/helper.service';
import { HelperModule } from '../helper/helper.module';

describe('CacheService', () => {
  let service: CacheService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HelperModule],
      providers: [
        CacheService,
        HelperService,
        {
          provide: 'CCM',
          useValue: {}
        }
      ],
    }).compile();

    service = module.get<CacheService>(CacheService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
