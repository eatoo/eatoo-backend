import { Injectable, Logger } from '@nestjs/common';
import { RedisClient, RedisError } from 'redis';
import { promisify } from 'util';
import { AsyncBridge } from './async-bridge.interface';
import { CTX } from '../constants/cache.constant';
import { HelperService } from '../helper/helper.service';

@Injectable()
export class CacheService {
    private readonly redis: RedisClient;
    private readonly asyncBridge: AsyncBridge;
    private configExpireType: string;
    private configExpireTime: number;
    
    constructor(private readonly helper: HelperService) {
        this.configExpireType = helper.conf('cache.expireType', 'EX')
        this.configExpireTime = helper.conf('cache.expireTime', 60 * 60 * 24 * 7)
        if (helper.conf('cache.enabled', true)) {
            this.redis = new RedisClient({
                host: helper.envOrConf('CACHE_HOST', 'cache.server.host', '127.0.0.1'),
                port: parseInt(helper.envOrConf('CACHE_PORT', 'cache.server.port', 6379), 10),
            });
            this.redis
            .on('error', this._onError)
            .on('connect', this._onConnect)
            .on('end', this._onEnd);
            this.asyncBridge = {
                get: promisify(this.redis.get).bind(this.redis),
                set: promisify(this.redis.set).bind(this.redis),
                incr: promisify(this.redis.incr).bind(this.redis),
                decr: promisify(this.redis.decr).bind(this.redis),
                hmset: promisify(this.redis.hmset).bind(this.redis),
                del: promisify(this.redis.del).bind(this.redis)
            };
        } else {
            this.asyncBridge = {
                get: <T>(): Promise<T> => Promise.resolve(null),
                set: (): Promise<string> => Promise.resolve(null),
                incr: (): Promise<string> => Promise.resolve(null),
                decr: (): Promise<string> => Promise.resolve(null),
                hmset: (): Promise<string> => Promise.resolve(null),
                del: (): Promise<number> => Promise.resolve(null)
            };
        }
    }
    
    public get<T>(key: string): Promise<T> {
        return this.asyncBridge.get<T>(key);
    }
    
    public set(
        key: string,
        value: any,
        expireType?: string,
        expireTime?: number
    ): Promise<string> {
        return this.asyncBridge.set(
            key,
            value,
            expireType ? expireType : this.configExpireType,
            expireTime ? expireTime : this.configExpireTime
        );
    }

    public del(...opts: string[]): Promise<number> {
        return this.asyncBridge.del(...opts);
    }
            
    private _onError(error: RedisError): void {
        Logger.error(error.message, error.stack, CTX);
    }
    
    private _onConnect(): void {
        Logger.verbose('Connected to the database.', CTX);
    }
    
    private _onEnd(): void {
        Logger.warn('Database connection closed.', CTX);
    }
}
