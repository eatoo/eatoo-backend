import * as fs from 'fs';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import * as rateLimit from 'express-rate-limit';
import * as helmet from 'helmet';
import { AppModule } from './app.module';
import { conf, envOrConf } from './helper/helpers.methods';
import { LoggingInterceptor } from './logging.interceptor';

const setupSwagger = app => {
  const options = new DocumentBuilder()
    .setTitle(conf('plugins.swagger.title', 'Eatoo'))
    .setVersion(process.env.SWAGGER_VERSION || process.env.npm_package_version)
    .addTag('auth')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(
    conf('plugins.swagger.path', 'doc'),
    app,
    document,
    {
      customCss: fs.readFileSync('swagger/theme.css').toString(),
    },
  );
};

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new LoggingInterceptor());
  app.enableCors({
    origin: [
      'https://eatoo.tech',
      'localhost',
      '104.40.142.56',
      '137.117.147.116',
      '51.144.126.116',
      '137.117.164.59',
      '51.136.57.161'
    ]
  });
  app.use(helmet());
  app.use(rateLimit({
    windowMs: conf('plugins.rateLimit.windowMs', 900000),
    max: conf('plugins.rateLimit.max', 1000),
  }));
  
  setupSwagger(app);
  
  const port = envOrConf('PORT', 'listenPort', 3000);
  await app.listen(port);
}

bootstrap();
