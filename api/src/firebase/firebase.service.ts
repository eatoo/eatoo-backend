import { Injectable, InternalServerErrorException, Scope } from '@nestjs/common';
import { Bucket, Storage } from '@google-cloud/storage'
import * as admin from 'firebase-admin';

var serviceAccount = require("../../serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://eatoo-backend.firebaseio.com',
  storageBucket: 'gs://eatoo-backend.appspot.com'
})

@Injectable({ scope: Scope.DEFAULT })
export class FirebaseService {
  bucket: Bucket
  constructor () {
    this.bucket = admin.storage().bucket()
  }

  storeFile (fileData: any) {
    const gcsname = 'user-thumbnail/' + Date.now() + fileData.originalname
    const file = this.bucket.file(gcsname)
    const stream = file.createWriteStream({
      metadata: { contentType: fileData.mimetype },
      resumable: false
    })
    stream.on('error', err => {
      throw new InternalServerErrorException('An error occured while uploading the file', err.toString())
    })
    stream.on('finish', () => {
      file.makePublic().then(() => {
        console.log('OK !!')
      })
    })
    stream.end(fileData.buffer)
  }
}
