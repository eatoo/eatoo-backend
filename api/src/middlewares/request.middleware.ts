import { Injectable, NestMiddleware } from '@nestjs/common';
import { LoggerService } from '../microservices/logger/logger.service';
import { EVENTS } from '../constants/logger.constant';

@Injectable()
export class RequestMiddleware implements NestMiddleware {
  
  constructor(private readonly loggerService: LoggerService) {
  }
  use(req: any, res: any, next: () => void) {
    if (req.method === 'GET' && req.url === '/') {
      next();
    } else {
      const data = {
        method: req.method,
        url: req.url,
        host: req.hostname,
        body: req.body
      }
      this.loggerService.emit<void>(EVENTS.REQUEST, {data: data});
      next();
    }
  }
}
