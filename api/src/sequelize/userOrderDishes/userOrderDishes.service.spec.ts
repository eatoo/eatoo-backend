import { Test, TestingModule } from '@nestjs/testing';
import { UserOrderDishesService } from './userOrderDishes.service';
import { JwtModule } from '@nestjs/jwt';
import { conf } from '../../helper/helpers.methods';
import { HelperModule } from '../../helper/helper.module';
import { CacheModule } from '../../cache/cache.module';
import { Order } from './entity/order.entity';
import { getModelToken } from '@nestjs/sequelize';
import { Dish } from '../userDishes/entity/dish.entity';

describe('UserDishesService', () => {
  let service: UserOrderDishesService;

  beforeEach(async () => {
    const order = {
      userId: 1,
      dishID: 1,
    };
    const updateDish = {
      isAvailable: false
    }
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: conf('plugins.jwt.secret', 'secret'),
          signOptions: {
            expiresIn: conf('plugins.jwt.expiresIn', 3600),
          },
        }),
        CacheModule,
        HelperModule
      ],
      providers: [
        UserOrderDishesService,
        {
          provide: getModelToken(Order),
          useValue: order
        },
        {
          provide: getModelToken(Dish),
          useValue: updateDish
        }
      ],
    }).compile();

    service = module.get<UserOrderDishesService>(UserOrderDishesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
