import { IsNotEmpty, IsNumberString, Matches, IsString } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class OrderDishDto {
  readonly id: number;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly sellerId: number;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly dishId: number;

  }


