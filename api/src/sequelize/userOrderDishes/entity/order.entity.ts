import { Table, Column, Model, DataType, CreatedAt, UpdatedAt, DeletedAt, Sequelize, ForeignKey, HasMany, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { IDefineOptions } from 'sequelize-typescript/lib/interfaces/IDefineOptions';
import { User} from '../../user/entity/user.entity';
import { Dish} from '../../userDishes/entity/dish.entity';

  const tableOptions: IDefineOptions = {
    tableName: 'UsersOrderDishes',
    timestamps: true
  } as IDefineOptions;
  
  @Table(tableOptions)
  export class Order extends Model<Order> {

    @Column({
      type: DataType.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true,
    })
    id: number;

    @ForeignKey(() => User)
    @Column({
      type: DataType.INTEGER,
      allowNull: false,
    })
    sellerId: number;
    
    @BelongsTo(() => User, 'sellerId')
    seller: User

    @ForeignKey(() => Dish)
    @Column({
      allowNull: false,
    })
    dishId: number;

    @ForeignKey(() => User)
    @Column({
      allowNull: false,
    })
    buyerId: number;

    @BelongsTo(() => User, 'buyerId')
    buyer: User

    @BelongsTo(() => Dish)
    dish: Dish
     
    @CreatedAt public createdAt: Date;
  
    @UpdatedAt public updatedAt: Date;
  }

  
