import { SequelizeModule } from '@nestjs/sequelize';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { UserOrderDishesController } from './userOrderDishes.controller';
import { UserOrderDishesService } from './userOrderDishes.service';
import { Order } from './entity/order.entity';
import { UserDishModule } from '../userDishes/userDishes.module';

@Module({
  imports: [
    UserDishModule,
    SequelizeModule.forFeature([ Order ]),
    PassportModule.register({ defaultStrategy: 'jwt', property: 'user' }),
  ],
  controllers: [UserOrderDishesController],
  providers: [UserOrderDishesService],
  exports: [SequelizeModule, UserOrderDishesService, PassportModule]
})
export class UserOrderDishModule {}