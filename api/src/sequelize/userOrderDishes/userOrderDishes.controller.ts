import { Controller, Get, Response, HttpStatus, HttpException, HttpCode, Param, Body, Post, UnauthorizedException,UseGuards, Request, Patch, Delete, Header } from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiExcludeEndpoint,
    ApiForbiddenResponse,
    ApiInternalServerErrorResponse,
    ApiResponse,
    ApiUnauthorizedResponse,
    ApiUseTags,
  } from '@nestjs/swagger';
import { UserOrderDishesService } from './userOrderDishes.service';
import { AuthGuard } from '@nestjs/passport';
import { OrderDishDto } from './dto/orderDishDto';
import { CurrentUser } from '../user/decorators/current-user.decorator';
import { LoggerService } from '../../microservices/logger/logger.service';
import { EVENTS } from '../../constants/logger.constant';
import { User } from '../user/entity/user.entity';

@ApiUseTags('orders')
@Controller('orders')
export class UserOrderDishesController {
    constructor(
        private readonly userOrderDishesService: UserOrderDishesService,
        private readonly loggerService: LoggerService,
    ) {
    }
    
    /**
    * ! POST /orders/order
    * @param dto 
    */
   @UseGuards(AuthGuard())
   @ApiBearerAuth()
   @ApiResponse({ status: 201, description: 'Returns the order dish.' })
   @ApiBadRequestResponse({ description: 'Bad request. Data\'s bad format.' })
   @ApiInternalServerErrorResponse({ description: 'An error occurred on our side.' })
   @Post('create')
   public async orderDish(@Body() orderDishDto: OrderDishDto, @CurrentUser() user: User) {
    const data = await this.userOrderDishesService.order(orderDishDto, user.id);
    return data;
    }
  
   /**
   * ! DELETE /orders/deleteDish/:id
   * Route only available for authenticated user
   */
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'Delete successful' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized.' })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  @HttpCode(200)
  @Delete('deleteOrder/:id')
    public async deleteOrder(@Param() param) {
    const data = await this.userOrderDishesService.delete(param.id);
    const res = {
        message: "Delete successfully."
    }
    if (data === 0) {
        throw new HttpException('You can\'t modify a dish who doesn\'t exist.', HttpStatus.BAD_REQUEST);
    }
    return res;
    }

    /**
    * ! GET /orders/all
    */  
   @UseGuards(AuthGuard())
   @ApiBearerAuth()
   @ApiResponse({ status: 200, description: 'Returns the orderDishes list' })
   @HttpCode(200)
   @Get('all')
   public async getOrders() {
        const data = await this.userOrderDishesService.findAll();
        return data;
    }

    /* TEST */

    @Get('/:id')
    public async getOrder(@Response() res, @Param() param) {
        const data = await this.userOrderDishesService.findById(param.id);
        return res.status(HttpStatus.OK).json(data);
    }
}