import { Test, TestingModule } from '@nestjs/testing';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { PassportModule } from '@nestjs/passport';
import { conf } from '../../helper/helpers.methods';
import { LoggerService } from '../../microservices/logger/logger.service';
import { logger as c } from '../../constants/index';
import { HelperModule } from '../../helper/helper.module';
import { CacheModule } from '../../cache/cache.module';
import { UserOrderDishesController } from './userOrderDishes.controller';
import { UserOrderDishesService } from './userOrderDishes.service';
import { Order } from './entity/order.entity';
import { getModelToken } from '@nestjs/sequelize';
import { Dish } from '../userDishes/entity/dish.entity';

describe('User Controller', () => {
  let controller: UserOrderDishesController;

  beforeEach(async () => {
    const order = {
      userId: 1,
      dishID: 1,
    };
    const updateDish = {
      isAvailable: false
    }
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule.register({ defaultStrategy: 'jwt', property: 'user' }),
        ClientsModule.register([
          {
            name: c.SERVICE_NAME,
            transport: Transport.TCP,
            options: {
              host: conf(c.CONF_KEYS.host, '127.0.0.1'),
              port: conf(c.CONF_KEYS.port, 6373),
            },
          },
        ]),
        CacheModule,
        HelperModule,
      ],
      controllers: [UserOrderDishesController],
      providers: [
        UserOrderDishesService,
        LoggerService,
        {
          provide: getModelToken(Order),
          useValue: order,
        },
        {
          provide: getModelToken(Dish),
          useValue: updateDish
        }
      ],
    }).compile();

    controller = module.get<UserOrderDishesController>(UserOrderDishesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
