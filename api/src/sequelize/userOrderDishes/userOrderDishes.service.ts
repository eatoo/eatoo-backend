import { HttpException, HttpStatus, Injectable, Inject} from '@nestjs/common';
import { OrderDishDto } from './dto/orderDishDto';
import { UpdateDishDto } from '../userDishes/dto/updateDishDto';
import { Order } from './entity/order.entity';
import { Dish } from '../userDishes/entity/dish.entity';
import { User} from '../user/entity/user.entity';
import { InjectModel } from '@nestjs/sequelize';

@Injectable()
export class UserOrderDishesService {
  constructor(
    @InjectModel(Order) private readonly sequelizeRepository: typeof Order,
    @InjectModel(Dish) private readonly sequelizeRepositoryTwo: typeof Dish,
  ) {}

  async findAll(): Promise<Order[]> {
    return await this.sequelizeRepository.findAll<Order>({
      attributes: ['id', 'createdAt'],
      include: [
        {
          model: Dish,
          attributes: ['id', 'userId', 'dishName', 'quantity', 'price', 'ingredients', 'deliveryHour', 'isAvailable']
        },
        {
          model: User,
          as: 'seller',
          attributes: ['username', 'name', 'familyName']
        },
      ]
    });
  }

  async order(orderDishDto: OrderDishDto, userId): Promise<Order> {
    const newOrder = {
      buyerId: userId,
      sellerId: orderDishDto.sellerId,
      dishId: orderDishDto.dishId,
    }
    const updateDish = {
      isAvailable: false
    }
    await this.updateDish(newOrder.dishId, updateDish)
    return await this.sequelizeRepository.create<Order>(newOrder);
  }

  async updateDish(id: number, newValue): Promise<Dish | null> {
    return await this.sequelizeRepositoryTwo.findByPk<Dish>(id)
    .then(dish => {
      dish = this.changeData(dish, newValue);
      return dish.save({ returning: true });
    })
    .catch(() => {
      throw new HttpException('You can\'t modify a dish who doesn\'t exist.', HttpStatus.BAD_REQUEST);
    })
  }

  private changeData(dish: UpdateDishDto, newValue: UpdateDishDto): Dish {
    for (const key of Object.keys(dish['dataValues'])) {
      if (dish[key] !== newValue[key] && newValue[key] !== undefined) {
        dish[key] = newValue[key];
      }
    }
    return dish as Dish;
  }

  async delete(id: number): Promise<number> {
    return this.sequelizeRepository.destroy({
      where: { id },
    }); 
  }

  /* TEST */

  async findById(id: number): Promise<Order> {
    return await this.sequelizeRepository.findByPk(id, {
      attributes: ['id', 'createdAt'],
      include: [
        {
          model: Dish,
          attributes: ['id', 'userId', 'dishName', 'quantity', 'price', 'ingredients', 'deliveryHour', 'isAvailable']
        },
        {
          model: User,
          as: 'seller',
          attributes: ['username', 'name', 'familyName']
        }
      ]
    });
  }
}