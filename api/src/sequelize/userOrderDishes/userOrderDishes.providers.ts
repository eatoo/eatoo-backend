import { Order } from './entity/order.entity';
import { Dish } from '../userDishes/entity/dish.entity';

export const userOrderDishesProviders = [
  {
    provide: 'UserOrderDishesRepository',
    useValue: Order,
  },
  {
    provide: 'UserDishesRepository',
    useValue: Dish,
  },
];