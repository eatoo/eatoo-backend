export enum Role {
  USER = 'USER',
  BUYER = 'BUYER',
  SELLER = 'SELLER',
  MODERATOR = 'MODERATOR',
  ADMIN = 'ADMIN',
  SUPERADMIN = 'SUPERADMIN',
}

export interface UserI {
  _id?: string;
  email: string;
  username: string;
  password: string;
  age?: string;
  name: string;
  familyName: string;
  phone: string;
  //roles: Role[];
}
