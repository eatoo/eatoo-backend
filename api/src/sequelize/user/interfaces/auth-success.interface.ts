import { UserI } from './user.interface';

export interface AuthSuccess {
  token: string;
  user: UserI;
}
