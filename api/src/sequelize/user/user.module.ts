import { SequelizeModule } from '@nestjs/sequelize';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './z-jwt.strategy';
import { conf } from '../../helper/helpers.methods';
import { CacheModule } from '../../cache/cache.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { User } from './entity/user.entity';

@Module({
  imports: [
    SequelizeModule.forFeature([ User ]),
    PassportModule.register({ defaultStrategy: 'jwt', property: 'user' }),
    JwtModule.register({
      secret: conf('plugins.jwt.secret', 'secret'),
      signOptions: {
        expiresIn: conf('plugins.jwt.expiresIn', 3600),
      },
    }),
    CacheModule
  ],
  controllers: [UserController],
  providers: [
    UserService,
    JwtStrategy,
    /* FirebaseService */
  ],
  exports: [SequelizeModule, UserService, PassportModule]
})
export class UserModule {}