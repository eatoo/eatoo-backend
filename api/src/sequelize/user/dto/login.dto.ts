import { IsEmail, IsNotEmpty, Length, IsOptional } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class LoginDto {
  @ApiModelPropertyOptional({minimum: 3})
  @Length(3)
  @IsOptional()
  readonly username?: string;

  @ApiModelPropertyOptional()
  @IsEmail()
  @IsOptional()
  readonly email?: string;

  @ApiModelProperty({minimum: 6})
  @IsNotEmpty()
  @Length(6)
  readonly password: string;
}
