import { IsEmail, IsNotEmpty, IsOptional, MaxLength, MinLength, IsNumberString, ValidateNested, IsString } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class UpdateUserDto {
  @IsOptional()
  username: string
  @IsOptional()
  password: string

  @ApiModelPropertyOptional()
  @IsEmail()
  @IsOptional()
  readonly email?: string;
  
  @ApiModelPropertyOptional({minimum: 10, maximum: 10})
  @MaxLength(10)
  @MinLength(10)
  @IsNotEmpty()
  @IsOptional()
  @IsNumberString()
  readonly phone?: string;
  
  @ApiModelPropertyOptional()
  @IsOptional()
  @IsNumberString()
  @MaxLength(3)
  @MinLength(2)
  readonly age?: string;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsString()
  readonly name?: string;

  @ApiModelPropertyOptional()
  @IsOptional()
  @IsString()
  readonly familyName?: string;

}
