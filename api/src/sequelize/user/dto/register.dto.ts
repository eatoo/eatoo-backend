import { IsEmail, IsNotEmpty, Length, IsMobilePhone } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class RegisterDto {
  @ApiModelProperty({minimum: 3})
  @IsNotEmpty()
  @Length(3)
  readonly username: string;

  @ApiModelProperty({minimum: 3})
  @IsNotEmpty()
  @Length(3)
  readonly name: string;

  @ApiModelProperty({minimum: 3})
  @IsNotEmpty()
  @Length(3)
  readonly familyName: string;

  @ApiModelProperty()
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

  @ApiModelProperty({minimum: 6})
  @IsNotEmpty()
  @Length(6)
  readonly password: string;

  @ApiModelProperty({minimum: 10, maximum: 10})
  @IsNotEmpty()
  @IsMobilePhone('fr-FR')
  readonly phone: string;

  // readonly thumbnail: any;
}
