import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { JwtModule } from '@nestjs/jwt';
import { conf } from '../../helper/helpers.methods';
import { HelperModule } from '../../helper/helper.module';
import { CacheModule } from '../../cache/cache.module';
import { User } from './entity/user.entity';
import { getModelToken } from '@nestjs/sequelize';

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {
    const user = {
      username: "Tima",
      email: "Tim@skate.fr",
      name: "Tim",
      familyName: "Seb",
      password: "jesuisunmpwd",
      phone: "0615780942"
    };
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: conf('plugins.jwt.secret', 'secret'),
          signOptions: {
            expiresIn: conf('plugins.jwt.expiresIn', 3600),
          },
        }),
        CacheModule,
        HelperModule
      ],
      providers: [
        UserService,
        {
          provide: getModelToken(User),
          useValue: user
        }
      ],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
