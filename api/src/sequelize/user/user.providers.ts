import { User } from './entity/user.entity';

export const userProviders = [
  {
    provide: 'UserRepository',
    useValue: User,
  },
];