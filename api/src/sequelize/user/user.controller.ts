import { Controller, Get, Response, HttpStatus, HttpCode, Param, Body, Post, UnauthorizedException,UseGuards, Request, Patch, Delete, Query } from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiExcludeEndpoint,
    ApiForbiddenResponse,
    ApiInternalServerErrorResponse,
    ApiResponse,
    ApiUnauthorizedResponse,
    ApiUseTags,
  } from '@nestjs/swagger';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';
import { RegisterDto } from './dto/register.dto';
import { LoginDto } from './dto/login.dto';
import { Role, UserI } from './interfaces/user.interface';
import { User } from './entity/user.entity';
import { UpdateUserDto } from './dto/updateUser.dto';
import { AuthSuccess } from './interfaces/auth-success.interface';
import { RolesGuard } from './guards/roles.guard';
import { CurrentUser } from './decorators/current-user.decorator';
import { LoggerService } from '../../microservices/logger/logger.service';
import { EVENTS } from '../../constants/logger.constant';
import { Dish } from '../userDishes/entity/dish.entity';

@ApiUseTags('auth')
@Controller('auth')
export class UserController {
    constructor(
        private readonly userService: UserService,
        private readonly loggerService: LoggerService,
    ) {
    }
    
   /**
   * ! POST /USERS/REGISTER
   * @param dto
   */
  @ApiResponse({ status: 201, description: 'Successful registration' })
  @ApiBadRequestResponse({ description: 'Bad request. Email or username has already been taken.' })
  @ApiInternalServerErrorResponse({ description: 'An error occurred on our side.' })
  @HttpCode(201)
  @Post('register')
  async register(@Body() dto: RegisterDto): Promise<AuthSuccess> {
    const user = await this.userService.register(dto);
    this.loggerService.emit<void>(EVENTS.POST_AUTH_REGISTER);
    return {
        token: await this.userService.signJwt(user),
        user: user,
    };
  }

   /**
   * ! POST /USERS/LOGIN
   * @param dto
   */
  @ApiResponse({ status: 200, description: 'Successful connection.' })
  @ApiBadRequestResponse({ description: 'Bad request.' })
  @ApiUnauthorizedResponse({ description: 'Authentication failed.' })
  @HttpCode(200)
  @Post('login')
  async login(@Body() dto: LoginDto): Promise<AuthSuccess> {
    const user = await this.userService.login(dto);
    if (!user) {
      throw new UnauthorizedException('Authentication failed.');
    }
    this.loggerService.emit<void>(EVENTS.POST_AUTH_LOGIN);
    return {
        token: await this.userService.signJwt(user),
        user: user,
    };
  }

   /**
   * ! GET /USERS/ME
   * Route only available for authenticated user
   */
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'Returns the user corresponding to the provided JWT.' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized.' })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  @HttpCode(200)
  @Get('me')
  async protected(@CurrentUser() user: User, @Query('dishes') dishes: string, @Query('orders') orders: string): Promise<User> {
    if (dishes !== undefined) {
      //@ts-ignore
      user['dataValues'].dishes = await user.getDishes({
        attributes: {exclude: ['updatedAt']}
      })
    }
    if (orders !== undefined) {
      //@ts-ignore
      user['dataValues'].orders = await user.getOrders({
        attributes: ['id', 'createdAt'],
        include: [
          {
            model: Dish,
            attributes: ['id', 'userId', 'dishName', 'quantity', 'price', 'ingredients', 'deliveryHour', 'isAvailable']
          },
          {
            model: User,
            as: 'seller',
            attributes: ['username', 'name', 'familyName']
          }
        ]
      })
    }
    return user;
  }

   /**
   * ! GET /USERS/
   */
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'Returns the users list' })
  @HttpCode(200)
  @Get('all')
  public async getUsers() {
    const data = await this.userService.findAll();
    return data;
  }

  /** 
  * ! PATCH /AUTH/ME
  * @param dto
  */
 @UseGuards(AuthGuard())
 @ApiBearerAuth()
 @ApiResponse({ status: 200, description: 'Returns the updated user' })
 @HttpCode(200)
 @Patch('me')
  public async updateUser(@CurrentUser() user: User, @Body() dto: UpdateUserDto): Promise<UserI> {
      const data = await this.userService.update(dto, user);
      return data;
  }
}