import { Test, TestingModule } from '@nestjs/testing';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { PassportModule } from '@nestjs/passport';
import { conf } from '../../helper/helpers.methods';
import { LoggerService } from '../../microservices/logger/logger.service';
import { logger as c } from '../../constants/index';
import { HelperModule } from '../../helper/helper.module';
import { CacheModule } from '../../cache/cache.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { User } from './entity/user.entity';
import { JwtModule } from '@nestjs/jwt';
import { getModelToken } from '@nestjs/sequelize';

describe('User Controller', () => {
  let controller: UserController;

  beforeEach(async () => {
    const user = {
      username: "Tima",
      email: "Tim@skate.fr",
      name: "Tim",
      familyName: "Seb",
      password: "jesuisunmpwd",
      phone: "0615780942"
    };
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: 'testsecret',
          signOptions: {
            expiresIn: 360000,
          },
        }),
        PassportModule.register({ defaultStrategy: 'jwt', property: 'user' }),
        ClientsModule.register([
          {
            name: c.SERVICE_NAME,
            transport: Transport.TCP,
            options: {
              host: conf(c.CONF_KEYS.host, '127.0.0.1'),
              port: conf(c.CONF_KEYS.port, 6373),
            },
          },
        ]),
        CacheModule,
        HelperModule,
      ],
      controllers: [UserController],
      providers: [
        UserService,
        LoggerService,
        {
          provide: getModelToken(User),
          useValue: user,
        },
      ],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
