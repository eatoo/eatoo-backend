import { HttpException, HttpStatus, Injectable, Inject} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { UpdateUserDto } from './dto/updateUser.dto';
import { UserI } from './interfaces/user.interface';
import { User } from './entity/user.entity';
import { Dish } from '../userDishes/entity/dish.entity';
import { Order } from '../userOrderDishes/entity/order.entity';

import { CacheService } from '../../cache/cache.service';
import sequelize = require('sequelize');
import { or } from 'sequelize';
import { empty } from 'rxjs';
import { InjectModel } from '@nestjs/sequelize';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User) private readonly sequelizeRepository: typeof User,
    private readonly jwtService: JwtService,
    private readonly cacheService: CacheService
  ) {}

  async register(registerDto: RegisterDto): Promise<User> {
    const newUser = {
      username: registerDto.username,
      email: registerDto.email,
      password: registerDto.password,
      phone: registerDto.phone,
      name: registerDto.name,
      familyName: registerDto.familyName,
    }

    return await this.sequelizeRepository.findAll<User>({ where: {
      username: registerDto.username
    }})
    .then(user => {
      if (user.length === 0) {
        const hash = bcrypt.hashSync(registerDto.password, 10);
        newUser.password = hash;
        return this.sequelizeRepository.create<User>(newUser);
      } else {
        throw new HttpException('This email or username is already taken.', HttpStatus.BAD_REQUEST)
      }
    })
  }

  async login(data: LoginDto): Promise<User> {
    const fieldName = data.email ? 'email' : (data.username) ? 'username' : null;
    if (fieldName == null) {
      throw new HttpException('You must specify either an email or a username.', HttpStatus.BAD_REQUEST);
    }
    return await this.sequelizeRepository.findAll<User>({ where: {
       [fieldName]: data[fieldName]
    }})
    .then(user => {
      if (!bcrypt.compareSync(data.password, user[0]['dataValues'].password)) {
        throw new HttpException(`Incorrect username or password.`, HttpStatus.UNAUTHORIZED);
      } else
        this.cacheService.set(user[0].id.toString(), JSON.stringify(user));
        return user[0] as User;
    })
    .catch(() => {
      throw new HttpException(`Incorrect username or password.`, HttpStatus.UNAUTHORIZED);
    })
  }

  async findAll(): Promise<User[]> {
    return await this.sequelizeRepository.findAll<User>();
  }

  async update(newValue: UpdateUserDto, user: User): Promise<User | null> {
    //this.cacheService.del(user._id);
    return await this.sequelizeRepository.findByPk<User>(user.id)
    .then(user => {
      newValue.username = undefined
      newValue.password = undefined;
      user = this.changeData(user, newValue);
      return user.save({ returning: true });
    })
    .catch(() => {
      throw new HttpException('You can\'t modify someone who doesn\'t exist.', HttpStatus.BAD_REQUEST);
    })
  }

  private changeData(user: UpdateUserDto, newValue: UpdateUserDto): User {
    for (const key of Object.keys(user['dataValues'])) {
      if (user[key] !== newValue[key] && newValue[key] !== undefined) {
        user[key] = newValue[key];
      }
    }
    return user as User;
  }

  async signJwt(user: User): Promise<string> {
    return this.jwtService.sign({
      id: user.id,
      username: user.username,
    });
  }

  async validateJwt(payload: JwtPayload): Promise<User> {
    const userFromCacheStr = await this.cacheService.get<string>(payload.id);
    const userFromCache = JSON.parse(userFromCacheStr || 'null');
    if (userFromCache) {
      console.log("Connecte via le cache !")
      return userFromCache
    };
   return this.sequelizeRepository.findByPk(payload.id, { attributes: {exclude: ['password', 'updatedAt']} })
   .then(user => {
      //users['dataValues'].dishes.forEach(dish => 
      //console.log(dish.dishName))
      if (user) {
        this.cacheService.set(payload.id.toString(), JSON.stringify(user));
        delete user.password
        //return user['dataValues'] as User;
        return user;
      }
    })
  }
}

/*
    const userFromDatabase = await this.sequelizeRepository.findAll({ where: {
      id: payload.id,
      username: payload.username,
    }})
    return userFromDatabase[0] as User;
  }
*/
