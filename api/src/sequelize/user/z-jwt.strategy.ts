import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserService } from './user.service';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { conf } from '../../helper/helpers.methods';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: conf('plugins.jwt.secret', 'secretKey'),
    });
  }
  
  async validate(payload: JwtPayload) {
    const user = await this.userService.validateJwt(payload);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
