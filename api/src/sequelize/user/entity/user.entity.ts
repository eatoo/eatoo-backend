import { Table, Column, Model, DataType, CreatedAt, UpdatedAt, DeletedAt, HasMany, ForeignKey } from 'sequelize-typescript';
import { IDefineOptions } from 'sequelize-typescript/lib/interfaces/IDefineOptions';
import { Dish } from '../../userDishes/entity/dish.entity';
import { Order } from '../../userOrderDishes/entity/order.entity';

  const tableOptions: IDefineOptions = {
    tableName: 'Users',
    timestamps: true
  } as IDefineOptions;
  
  @Table(tableOptions)
  export class User extends Model<User> {

    @Column({
      type: DataType.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true,
    })
    id: number;
  
    @Column({
      allowNull: false,
    })
    username: string;

    @Column({
      allowNull: false,
      validate: {
        isEmail: true,
      },
    })
    email: string;
  
    @Column({
      allowNull: false,
    })
    password: string;

    @Column({
      allowNull: false,
    })
    phone: string;

    @Column({
      allowNull: false,
    })
    name: string;

    @Column({
      allowNull: false,
    })
    familyName: string;

    @Column({
      allowNull: true,
    })
    age: string;

    @HasMany(() => Dish)
    dishes: Dish[];

    @HasMany(() => Order, 'buyerId')
    orders: Order[];

    @CreatedAt public createdAt: Date;
  
    @UpdatedAt public updatedAt: Date;
  }