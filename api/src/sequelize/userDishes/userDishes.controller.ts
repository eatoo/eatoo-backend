import { Controller, Get, Response, HttpStatus, HttpException, HttpCode, Param, Body, Post, UnauthorizedException,UseGuards, Request, Patch, Delete, Header } from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiExcludeEndpoint,
    ApiForbiddenResponse,
    ApiInternalServerErrorResponse,
    ApiResponse,
    ApiUnauthorizedResponse,
    ApiUseTags,
  } from '@nestjs/swagger';
import { UserDishesService } from './userDishes.service';
import { AuthGuard } from '@nestjs/passport';
import { CreateDishDto } from './dto/createDishDto';
import { UpdateDishDto } from './dto/updateDishDto';
import { CurrentUser } from '../user/decorators/current-user.decorator';
import { LoggerService } from '../../microservices/logger/logger.service';
import { EVENTS } from '../../constants/logger.constant';

@ApiUseTags('dishes')
@Controller('dishes')
export class UserDishesController {
    constructor(
        private readonly userDishesService: UserDishesService,
        private readonly loggerService: LoggerService,
    ) {
    }
    
   /**
    * ! POST /UsersDishes/create
    * @param dto 
    */
   @UseGuards(AuthGuard())
   @ApiBearerAuth()
   @ApiResponse({ status: 201, description: 'Returns the create dish.' })
   @ApiBadRequestResponse({ description: 'Bad request. Data\'s bad format.' })
   @ApiInternalServerErrorResponse({ description: 'An error occurred on our side.' })
   @Post('create')
   public async createDish(@Body() createDishDto: CreateDishDto, @CurrentUser() user) {
    const data = await this.userDishesService.create(createDishDto, user.id);
    return data;
    }
    
   /**
   * ! PATCH /UsersDishes/updateDish/:id
   * Route only available for authenticated user
   */
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'Returns the updated dish' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized.' })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  @HttpCode(200)
  @Patch('updateDish/:id')
  public async updateDish(@Body() dto: UpdateDishDto, @Param() param) {
      const data = await this.userDishesService.update(param.id, dto);
      return data;
  }

   /**
   * ! DELETE /UsersDishes/deleteDish/:id
   * Route only available for authenticated user
   */
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'Delete successful' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized.' })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  @HttpCode(200)
  @Delete('deleteDish/:id')
    public async deleteDish(@Param() param) {
    const data = await this.userDishesService.delete(param.id);
    const res = {
        message: "Delete successfully."
    }
    if (data === 0) {
        throw new HttpException('You can\'t modify a dish who doesn\'t exist.', HttpStatus.BAD_REQUEST);
    }
    return res;
    }

    /**
    * ! GET /UsersDishes/
    */  
   @UseGuards(AuthGuard())
   @ApiBearerAuth()
   @ApiResponse({ status: 200, description: 'Returns the dishes list' })
   @HttpCode(200)
   @Get('all')
  public async getDishes() {
        const data = await this.userDishesService.findAll();
        return data;
    }

    /* TEST */

    @Get('/:id')
    public async getDish(@Response() res, @Param() param) {
        const data = await this.userDishesService.findById(param.id);
        return res.status(HttpStatus.OK).json(data);
    }

}