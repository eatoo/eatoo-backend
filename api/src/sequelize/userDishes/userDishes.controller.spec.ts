import { Test, TestingModule } from '@nestjs/testing';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { PassportModule } from '@nestjs/passport';
import { conf } from '../../helper/helpers.methods';
import { LoggerService } from '../../microservices/logger/logger.service';
import { logger as c } from '../../constants/index';
import { HelperModule } from '../../helper/helper.module';
import { CacheModule } from '../../cache/cache.module';
import { UserDishesController } from './userDishes.controller';
import { UserDishesService } from './userDishes.service';
import { getModelToken } from '@nestjs/sequelize';
import { Dish } from './entity/dish.entity';

describe('User Controller', () => {
  let controller: UserDishesController;
  const dish = {
    userId: 1,
    dishName: "Pates à la carbonara",
    quantity: "1",
    price:"10.00",
    ingredients: "Carbonara;Pates",
    deliveryHour: "10:00",
    isAvailable: true
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule.register({ defaultStrategy: 'jwt', property: 'user' }),
        ClientsModule.register([
          {
            name: c.SERVICE_NAME,
            transport: Transport.TCP,
            options: {
              host: conf(c.CONF_KEYS.host, '127.0.0.1'),
              port: conf(c.CONF_KEYS.port, 6373),
            },
          },
        ]),
        CacheModule,
        HelperModule,
      ],
      controllers: [UserDishesController],
      providers: [
        UserDishesService,
        LoggerService,
        {
          provide: getModelToken(Dish),
          useValue: dish,
        },
      ],
    }).compile();

    controller = module.get<UserDishesController>(UserDishesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
