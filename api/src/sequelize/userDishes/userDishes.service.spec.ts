import { Test, TestingModule } from '@nestjs/testing';
import { UserDishesService } from './userDishes.service';
import { JwtModule } from '@nestjs/jwt';
import { conf } from '../../helper/helpers.methods';
import { HelperModule } from '../../helper/helper.module';
import { CacheModule } from '../../cache/cache.module';
import { Dish } from './entity/dish.entity';
import { getModelToken } from '@nestjs/sequelize';

describe('UserDishesService', () => {
  let service: UserDishesService;

  beforeEach(async () => {
    const dish = {
      userId: 1,
      dishName: "Pates à la carbonara",
      quantity: "1",
      price:"10.00",
      ingredients: "Carbonara;Pates",
      deliveryHour: "10:00",
      isAvailable: true
    };
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: conf('plugins.jwt.secret', 'secret'),
          signOptions: {
            expiresIn: conf('plugins.jwt.expiresIn', 3600),
          },
        }),
        CacheModule,
        HelperModule
      ],
      providers: [
        UserDishesService,
        {
          provide: getModelToken(Dish),
          useValue: dish
        }
      ],
    }).compile();

    service = module.get<UserDishesService>(UserDishesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
