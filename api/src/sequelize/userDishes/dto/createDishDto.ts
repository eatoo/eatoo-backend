import { IsNotEmpty, IsNumberString, Matches, IsString } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateDishDto {
  readonly id: number;

  @ApiModelProperty({minimum: 3})
  @IsNotEmpty()
  dishName: string;

  @ApiModelProperty({minimum: 1})
  @IsNotEmpty()
  @IsNumberString()
  quantity: string;

  @ApiModelProperty({minimum: 1})
  @IsNotEmpty()
  @Matches(/^([0-9]{0,2}((.)[0-9]{0,2}))$/)
  price: string;

  @ApiModelProperty({minimum: 1})
  @IsNotEmpty()
  @IsString({each: true})
  //ingredients: string[];
  ingredients: string;

  @ApiModelProperty({minimum: 1})
  @IsNotEmpty()
  @Matches(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/)
  deliveryHour: string;

  }


