import { IsNotEmpty, IsString, Matches, IsOptional, IsNumberString } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class UpdateDishDto {

  @ApiModelPropertyOptional({minimum: 3})
  @IsOptional()
  dishName: string;

  @ApiModelPropertyOptional({minimum: 1})
  @IsOptional()
  @IsNumberString()
  quantity: string;

  @ApiModelPropertyOptional({minimum: 1})
  @IsOptional()
  @Matches(/^([0-9]{0,2}((.)[0-9]{0,2}))$/)
  price: string;

  @ApiModelPropertyOptional({minimum: 1})
  @IsOptional()
  @IsString({each: true})
  //ingredients: string[];
  ingredients: string;

  @ApiModelPropertyOptional({minimum: 1})
  @IsOptional()
  @Matches(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/)
  deliveryHour: string;

  }