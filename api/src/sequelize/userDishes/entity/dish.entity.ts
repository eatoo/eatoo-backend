import { Table, Column, Model, DataType, CreatedAt, UpdatedAt, DeletedAt, Sequelize, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { IDefineOptions } from 'sequelize-typescript/lib/interfaces/IDefineOptions';
import { User} from '../../user/entity/user.entity';
import { Order } from '../../userOrderDishes/entity/order.entity';

  const tableOptions: IDefineOptions = {
    tableName: 'UsersDishes',
    timestamps: true
  } as IDefineOptions;
  
  @Table(tableOptions)
  export class Dish extends Model<Dish> {

    @Column({
      type: DataType.INTEGER,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      primaryKey: true,
    })
    id: number;

    @ForeignKey(() => User)
    @Column({
      type: DataType.INTEGER,
      allowNull: false,
    })
    userId: number;
    
    @Column({
      allowNull: false,
    })
    dishName: string;

    @Column({
      allowNull: false,
    })
    quantity: string;

    @Column({
      allowNull: false,
    })
    price: string;

    @Column({
      allowNull: false,
    })
    //ingredients: string[];
    ingredients: string;

    @Column({
      allowNull: false,
    })
    deliveryHour: string;

    @Column({
      allowNull: false,
    })
    isAvailable: Boolean;

    @HasMany(() => Order)
    order: Order[]

    @BelongsTo(() => User)
    seller: User

    @CreatedAt public createdAt: Date;
  
    @UpdatedAt public updatedAt: Date;
  }

  
