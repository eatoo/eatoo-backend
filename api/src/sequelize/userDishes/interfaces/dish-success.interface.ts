import { Dish } from './dish.interface';

export interface DishSuccess {
  dish: Dish;
}
