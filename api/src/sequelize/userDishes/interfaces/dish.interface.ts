export interface Dish {
    _id?: string;
    name: string;
    number: string;
    price: string;
    ingredients: string[];
    hour: string;
  }
  