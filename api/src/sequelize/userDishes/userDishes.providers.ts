import { Dish } from './entity/dish.entity';

export const userDishesProviders = [
  {
    provide: 'UserDishesRepository',
    useValue: Dish,
  },
];