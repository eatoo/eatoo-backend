import { SequelizeModule } from '@nestjs/sequelize';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { UserDishesController } from './userDishes.controller';
import { UserDishesService } from './userDishes.service';
import { Dish } from './entity/dish.entity';

@Module({
  imports: [
    SequelizeModule.forFeature([ Dish ]),
    PassportModule.register({ defaultStrategy: 'jwt', property: 'user' }),
  ],
  controllers: [UserDishesController],
  providers: [UserDishesService],
  exports: [SequelizeModule, UserDishesService, PassportModule]
})
export class UserDishModule {}