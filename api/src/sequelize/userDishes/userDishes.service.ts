import { HttpException, HttpStatus, Injectable, Inject} from '@nestjs/common';
import { CreateDishDto } from './dto/createDishDto';
import { UpdateDishDto } from './dto/updateDishDto';
import { Dish } from './entity/dish.entity';
import { User} from '../user/entity/user.entity';
import { InjectModel } from '@nestjs/sequelize';

@Injectable()
export class UserDishesService {
  constructor(
    @InjectModel(Dish) private readonly sequelizeRepository: typeof Dish,
  ) {}

  async findAll(): Promise<Dish[]> {
    return await this.sequelizeRepository.findAll<Dish>({
      include: [
        {
          model: User,
          attributes: ['username', 'name', 'familyName']
        }
      ]
    });
  }

  async create(createDishDto: CreateDishDto, userId): Promise<Dish> {
    const dish = {
      userId: userId,
      dishName: createDishDto.dishName,
      quantity: createDishDto.quantity,
      price: createDishDto.price,
      ingredients: createDishDto.ingredients,
      deliveryHour: createDishDto.deliveryHour,
      isAvailable: true
    }
    return await this.sequelizeRepository.create<Dish>(dish);
  }

  async update(id: number, newValue: UpdateDishDto): Promise<Dish | null> {
    return await this.sequelizeRepository.findByPk<Dish>(id)
    .then(dish => {
      dish = this.changeData(dish, newValue);
      return dish.save({ returning: true });
    })
    .catch(() => {
      throw new HttpException('You can\'t modify a dish who doesn\'t exist.', HttpStatus.BAD_REQUEST);
    })
  }

  private changeData(dish: UpdateDishDto, newValue: UpdateDishDto): Dish {
    for (const key of Object.keys(dish['dataValues'])) {
      if (dish[key] !== newValue[key] && newValue[key] !== undefined) {
        dish[key] = newValue[key];
      }
    }
    return dish as Dish;
  }

  async delete(id: number): Promise<number> {
    return this.sequelizeRepository.destroy({
      where: { id },
    }); 
  }

  /* TEST */

  async findById(id: number): Promise<Dish> {
    return await this.sequelizeRepository.findByPk(id, {
      include: [
        {
          model: User,
          attributes: ['username', 'name', 'familyName']
        }
      ]
    });
  }

}