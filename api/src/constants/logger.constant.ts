export const SERVICE_NAME = 'LOGGER_SERVICE';

export const LOG_CONTEXT = 'MS/Logger';

const makeKey = (key: string): string => 'logger.' + key;

export const CONF_KEYS = {
    connectionRetries: makeKey('connectionRetries'),
    enabled: makeKey('enabled'),
    debugFails: makeKey('debugFails'),
    host: makeKey('host'),
    envHost: 'MS_LOGGER_HOST',
    port: makeKey('port'),
    envPort: 'MS_LOGGER_PORT'
};

export const EVENTS = {
    POST_AUTH_LOGIN: "POST_AUTH_LOGIN",
    POST_AUTH_REGISTER: "POST_AUTH_REGISTER",
    GET_AUTH_ME: "GET_AUTH_ME",
    PATCH_AUTH_ME: "PATCH_AUTH_ME",
    POST_CREATE_DISH: "POST_CREATE_DISH",
    REQUEST: "REQUEST",
};
