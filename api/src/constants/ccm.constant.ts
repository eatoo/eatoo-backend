export const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm9maWxlSWQiOiIxNTU2NjdhNi01NzNmLTRlZTItYTM4MS1mMjlkZGY5ODg5M2IiLCJwcm9maWxlTmFtZSI6IlN1cGVyYWRtaW4iLCJpYXQiOjE1OTIxMzU0OTUsImV4cCI6MTYyMzY5MzA5NX0.adCwg_qinKC-ooEOwPB4TEraEwyj43l_5jJ39FHBH-Y'

export const API_UUID = '0f691d89-9b8c-4cd1-934e-6ca9eff64e71'

const BASE_URL = process.env.CCM_BASE_URL || 'https://ccm.eatoo.tech/api'

export const LOGIN_URL = BASE_URL + '/auth/login'
export const LOGIN_PROFILE = 'Superadmin'
export const LOGIN_PASSWORD = 'Superadmin'

export const BASE_RESOURCES_URL = BASE_URL + '/resources'
export const URL = `${BASE_RESOURCES_URL}/${API_UUID}/${process.env.NODE_ENV}?extended`
