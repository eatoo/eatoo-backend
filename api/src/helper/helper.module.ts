import { Global, Module, HttpModule, HttpService, Logger } from '@nestjs/common';
import { HelperService } from './helper.service';
import { assertConf } from './helpers.methods'
import { AxiosResponse } from 'axios';
import { Agent } from 'https'
import { first } from 'rxjs/operators'
import { ccm, helper } from '../constants'

const agent = new Agent({
  rejectUnauthorized: false
});

const CCM_Login = async (http: HttpService, url: string, profile: string, password: string): Promise<string> => {
  try {
    const ar: AxiosResponse = await http.post(url, { profile, password }, { httpsAgent: agent }).pipe(first()).toPromise();
    Logger.verbose('Login succeed', helper.CCM_CTX)
    return ar.data.access_token
  } catch (e) {
    Logger.error('Error while logging in to CCM server ' + process.env.NODE_ENV, e, helper.CCM_CTX)
    return null
  }
}

const CCM_FetchConfig = async (http: HttpService, url: string, token: string, loginIfFail?: boolean): Promise<object> => {
  try {
    const ar: AxiosResponse = await http.get(url, {
      headers: { 'Authorization': 'Bearer ' + token },
      httpsAgent: agent
    }).pipe(first()).toPromise();
    Logger.verbose('Successfully retrieved configuration from CCM server', helper.CCM_CTX)
    return ar.data
  } catch (e) {
    Logger.error('Error while retrieving CCM config for env ' + process.env.NODE_ENV, e, helper.CCM_CTX)
    if (loginIfFail === true) {
      Logger.verbose('Trying to login to CCM server and retrieve token', '')
      try {
        const newToken = await CCM_Login(http, ccm.LOGIN_URL, ccm.LOGIN_PROFILE, ccm.LOGIN_PASSWORD)
        return CCM_FetchConfig(http, url, newToken, false)
      } catch (e) {
        return {}
      }
    }
    Logger.verbose('Configuration will default to local config')
    return {}
  }
}

@Global()
@Module({
  imports: [HttpModule], 
  providers: [
    HelperService,
    {
      provide: 'CCM',
      useFactory: async (http: HttpService) => {
        if (assertConf('ccmEnabled', true)) {
          return await CCM_FetchConfig(http, ccm.URL, ccm.TOKEN, true)
        } else {
          return {}
        }
      },
      inject: [HttpService]
    }
  ],
  exports: [HelperService],
})
export class HelperModule {}
