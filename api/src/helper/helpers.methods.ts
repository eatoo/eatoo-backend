import * as config from 'config';

export const conf = (configKey: string, defaultValue: any): any => {
  return (config.has(configKey)) ? config.get(configKey) : defaultValue;
};

export const assertConf = (configKey: string, value: any): boolean => {
  return (config.has(configKey)) ? config.get(configKey) === value : false;
};

// @ts-ignore
export const envOrConf = (
  envKey: string,
  configKey: string,
  defaultValue: any = '',
): any => {
  return process.env[envKey] ? process.env[envKey] : conf(configKey, defaultValue);
};
