import { Injectable, Logger, Inject } from '@nestjs/common';
import * as config from 'config';
import { assertConf, conf, envOrConf } from './helpers.methods';

@Injectable()
export class HelperService {
  
  constructor(@Inject('CCM') private readonly ccm: object) {}

  fromCcm<T>(key: string): T {
    const value = this.ccm[key]
    if (value === undefined) {
      return this.envOrConf(key.replace(/./g, '_').toUpperCase(), key)
    }
    return this.ccm[key];
  }
  
  conf(configKey: string, defaultValue: any): any {
    return conf(configKey, defaultValue);
  }
  
  assertConf(configKey: string, value: any): boolean {
    return assertConf(configKey, value);
  }
  
  envOrConf(envKey: string, configKey: string, value: any = ''): any {
    return envOrConf(envKey, configKey, value);
  }
  
  log(message: string, context: string = '', isTimeDiffEnabled: boolean = false) {
    Logger.log(message, context, isTimeDiffEnabled);
  }
  verbose(message: string, context: string = '', isTimeDiffEnabled: boolean = false) {
    Logger.verbose(message, context, isTimeDiffEnabled);
  }
  warn(message: string, context: string = '', isTimeDiffEnabled: boolean = false) {
    Logger.warn(message, context, isTimeDiffEnabled);
  }
  error(
    message: string,
    trace: string = '',
    context: string = '',
    isTimeDiffEnabled: boolean = false,
  ) {
    Logger.error(message, trace, context, isTimeDiffEnabled);
  }
  debug(message: string, context: string = '', isTimeDiffEnabled: boolean = false) {
    Logger.debug(message, context, isTimeDiffEnabled);
  }
}
