import { Module, MiddlewareConsumer } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HelperModule } from './helper/helper.module';
import { LoggerModule } from './microservices/logger/logger.module';
import { CacheModule } from './cache/cache.module';
import { RequestMiddleware } from './middlewares/request.middleware';
import { UserModule } from './sequelize/user/user.module';
import { UserDishModule } from './sequelize/userDishes/userDishes.module';
import { UserOrderDishModule } from './sequelize/userOrderDishes/userOrderDishesOrder.module';
import { HelperService } from './helper/helper.service';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from './logging.interceptor';

@Module({
  imports: [
    LoggerModule,
    HelperModule,
    CacheModule,
    SequelizeModule.forRootAsync({
      imports: [HelperModule],
      useFactory: (helper: HelperService) => ({
        dialect: 'mysql',
        host: helper.fromCcm('database.host'),
        username: helper.fromCcm('database.username'),
        password: helper.fromCcm('database.password'),
        database: helper.fromCcm('database.db_name'),
        logging: helper.fromCcm('database.logging'),
        autoLoadModels: true,
        synchronize: true
      }),
      inject: [HelperService]
    }),
    UserModule,
    UserDishModule,
    UserOrderDishModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(RequestMiddleware).forRoutes('/');
  }
}
