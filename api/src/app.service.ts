import { Injectable } from '@nestjs/common';
import * as os from 'os';

@Injectable()
export class AppService {
  constructor() {}
  
  getHello(): string {
    return 'Hello World!';
  }

  getHost(): string {
    return os.hostname();
  }
}
