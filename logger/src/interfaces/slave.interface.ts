import { Payload } from './payload.interface';

export enum SlaveTypes {
  ALL = 0,
  REDIS = 1,
  FILE_REQUEST = 2,
  ERROR = 4,
}

export interface ISlave {
  process(event: string, payload: Payload): Promise<boolean>;
}

export interface Slave {
  name: string;
  instance: ISlave;
}
