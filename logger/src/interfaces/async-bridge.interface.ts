export interface AsyncBridge {
  get: <T>(key: string) => Promise<T>;
  set: (key: string, value: any, expireType?: string, expireTime?: number) => Promise<string>;
  incr: (key: string) => Promise<any>;
  decr: (key: string) => Promise<any>;
  hmset: (...opts: string[]) => Promise<any>;
  del: (...opts: string[]) => Promise<number>;
}
