import { Controller } from '@nestjs/common';
import { AppService } from './app.service';
import { RedisService } from './slaves/redis/redis.service';
import { FileRequestService } from './slaves/file-request/file-request.service';
import { EventPattern } from '@nestjs/microservices';
import { Payload, PayloadRedis } from '_interfaces/payload.interface';
import { SLAVES, EVENTS } from './constants';
import { SlaveTypes } from '_interfaces/slave.interface';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly redisService: RedisService,
    private readonly fileRequestService: FileRequestService,
  ) {
    appService.register({
      name: SLAVES.REDIS,
      instance: redisService,
    });
    appService.register({
      name: SLAVES.FILE,
      instance: fileRequestService,
    });
  }
  
  @EventPattern(EVENTS.POST_AUTH_LOGIN)
  postAuthLogin(payload: Payload): Promise<boolean> {
    return this.appService.process(EVENTS.POST_AUTH_LOGIN, payload);
  }
  
  @EventPattern(EVENTS.POST_AUTH_REGISTER)
  postAuthRegister(payload: Payload): Promise<boolean> {
    return this.appService.process(EVENTS.POST_AUTH_REGISTER, payload);
  }
  
  @EventPattern(EVENTS.GET_AUTH_ME)
  getAuthMe(payload: Payload): Promise<boolean> {
    return this.appService.process(EVENTS.GET_AUTH_ME, payload);
  }
  
  @EventPattern(EVENTS.PATCH_AUTH_ME)
  patchAuthMe(payload: Payload): Promise<boolean> {
    return this.appService.process(EVENTS.PATCH_AUTH_ME, payload);
  }

  @EventPattern(EVENTS.POST_CREATE_DISH)
  postCreateDish(payload: Payload): Promise<boolean> {
    return this.appService.process(EVENTS.POST_CREATE_DISH, payload);
  }

  @EventPattern(EVENTS.REQUEST)
  request(payload: Payload): Promise<boolean> {
    return this.appService.processFileRequest(EVENTS.REQUEST, payload);
  }
}
