import { Injectable } from '@nestjs/common';
import { Slave, SlaveTypes } from '_interfaces/slave.interface';
import { Payload, PayloadRedis } from '_interfaces/payload.interface';
import { SLAVES } from './constants';

@Injectable()
export class AppService {
  
  private slaves: Slave[] = [];
  
  constructor() {
  }
  
  register(slave: Slave): void {
    this.slaves.push(slave);
  }

  async process<T>(event, payload: Payload): Promise<boolean> {
    const asyncStatus: Promise<boolean>[] = this.slaves.map(slave => slave.instance.process(event, payload));
    const status = await Promise.all(asyncStatus);
    return status.some(status => status);
  }

  async processFileRequest<T>(event, payload: Payload): Promise<boolean> {
    return this.slaves.find((slave: Slave) => slave.name === SLAVES.FILE).instance.process(event, payload);
  }
}
