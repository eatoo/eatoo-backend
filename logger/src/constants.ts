export const SLAVES = {
  REDIS: 'REDIS',
  FILE: 'FILE'
};

export const EVENTS = {
  POST_AUTH_REGISTER: 'POST_AUTH_REGISTER',
  POST_AUTH_LOGIN: 'POST_AUTH_LOGIN',
  GET_AUTH_ME: 'GET_AUTH_ME',
  PATCH_AUTH_ME: 'PATCH_AUTH_ME',
  POST_CREATE_DISH: 'POST_CREATE_DISH',
  REQUEST: 'REQUEST',
};

export const CTX = {
  REDIS: 'SLAVE/REDIS'
};

export enum DateIncrement {
  DAY,
  MONTH,
  YEAR,
}

export const DateIncrementDef: object = {
  d: DateIncrement.DAY,
  m: DateIncrement.MONTH,
  y: DateIncrement.YEAR,
};

export const DateIncrementKeys: object = {
  [DateIncrement.YEAR]: 'YYYY',
  [DateIncrement.MONTH]: 'YYYY-MM',
  [DateIncrement.DAY]: 'YYYY-MM-DD',
}