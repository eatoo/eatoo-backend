import { Module } from '@nestjs/common';
import { FileRequestService } from './file-request.service';

@Module({
  providers: [FileRequestService],
  exports: [FileRequestService],
})
export class FileRequestModule {}
