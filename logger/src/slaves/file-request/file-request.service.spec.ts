import { Test, TestingModule } from '@nestjs/testing';
import { FileRequestService } from './file-request.service';
import { ConfigModule } from '../../config/config.module';

describe('FileRequestService', () => {
  let service: FileRequestService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      providers: [FileRequestService],
    }).compile();

    service = module.get<FileRequestService>(FileRequestService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
