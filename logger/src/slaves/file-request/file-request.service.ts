import { Injectable, Logger } from '@nestjs/common';
import { Payload } from '_interfaces/payload.interface';
import { ISlave } from '_interfaces/slave.interface';
import { appendFile} from 'fs';
import { ConfigService } from '../../config/config.service';
import * as moment from 'moment';
import { EVENTS } from '../../constants';

@Injectable()
export class FileRequestService implements ISlave {
  private readonly folderName: string;

  constructor(config: ConfigService) {
    this.folderName = config.get('slaves.fileRequest.folderName', 'logs');
    if (!this.folderName.endsWith('/')) this.folderName += '/';
  }

  async process(event: string, payload: Payload): Promise<boolean> {
    
    if (event !== EVENTS.REQUEST) return Promise.resolve(true);

    Logger.debug('Log generated', 'SLAVE/FILE_REQUEST');

    appendFile(this._getFileName(), this._getFileContent(payload.data), {
      flag: 'as'
    }, function (err) {
      if (err) throw err;
        console.log('File created !');
    });

    return true;
  }

  private _getFileName(): string {
    let fileName: string = this.folderName + "log-" + moment().format('YYYY-MM-DD') + ".txt";
    return fileName;
  }

  private _getFileContent(data): string {
    let content: string = "[" + moment().format('YYYY-MM-DD, h:mm:ss a') + "] [host : " + data.host + " ] [method : " + data.method + " ] [url : " + data.url + " ] [data :" + JSON.stringify(data.body) + " ] \n"; 
    return content;
  }

}
