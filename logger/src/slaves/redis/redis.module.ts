import { Module } from '@nestjs/common';
import { RedisService } from './redis.service';
import { ClientService } from './client/client.service';

@Module({
  providers: [RedisService, ClientService],
  exports: [RedisService],
})
export class RedisModule {}
