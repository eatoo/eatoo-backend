import { Test, TestingModule } from '@nestjs/testing';
import * as moment from 'moment';
import { RedisService } from './redis.service';
import { ClientService } from './client/client.service';
import { ConfigService } from '../../config/config.service';
import { 
  DateIncrement,
  DateIncrementDef,
  DateIncrementKeys,
  EVENTS
}  from '../../constants';

describe('RedisService', () => {
  let service: RedisService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RedisService,
        ClientService,
        ConfigService
      ],
    }).compile();

    service = module.get<RedisService>(RedisService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('Parsing payload.dateIncrements', () => {

    it('should return all the increments if empty', () => {
      const increments = service._parseDateIncrements();
      expect(increments).toContain(DateIncrement.DAY);
      expect(increments).toContain(DateIncrement.MONTH);
      expect(increments).toContain(DateIncrement.YEAR);
    });

    it('should return correctly handle option = "y"', () => {
      const increments = service._parseDateIncrements('y');
      expect(increments).toContain(DateIncrement.YEAR);
      expect(increments).toEqual([DateIncrement.YEAR]);
    });
    
    it('should return correctly handle = "ym"', () => {
      const increments = service._parseDateIncrements('ym');
      expect(increments).toContain(DateIncrement.YEAR);
      expect(increments).toContain(DateIncrement.MONTH);
    });
    
    it('should return correctly handle = "ymd"', () => {
      const increments = service._parseDateIncrements('ymd');
      expect(increments).toContain(DateIncrement.YEAR);
      expect(increments).toContain(DateIncrement.MONTH);
      expect(increments).toContain(DateIncrement.DAY);
    });
    
    it('should return correctly handle = "myd" (random order)', () => {
      const increments = service._parseDateIncrements('myd');
      expect(increments).toContain(DateIncrement.YEAR);
      expect(increments).toContain(DateIncrement.MONTH);
      expect(increments).toContain(DateIncrement.DAY);
    });
    
  });

  describe('Generating the Redis key from a DateIncrement', () => {
    const now = moment();
    const YEAR = now.format(DateIncrementKeys[DateIncrement.YEAR]);
    const MONTH = now.format(DateIncrementKeys[DateIncrement.MONTH]);
    const DAY = now.format(DateIncrementKeys[DateIncrement.DAY]);

    it('simple : should handle GET_AUTH_ME with YEAR', () => {
      const key = service._getRedisKey(EVENTS.GET_AUTH_ME, DateIncrement.YEAR);
      expect(key).toBe(EVENTS.GET_AUTH_ME + '_' + YEAR);
    });

    it('simple : should handle POST_AUTH_REGISTER with MONTH', () => {
      const key = service._getRedisKey(EVENTS.POST_AUTH_REGISTER, DateIncrement.MONTH);
      expect(key).toBe(EVENTS.POST_AUTH_REGISTER + '_' + MONTH);
    });

    it('simple : should handle POST_AUTH_LOGIN with DAY', () => {
      const key = service._getRedisKey(EVENTS.POST_AUTH_LOGIN, DateIncrement.DAY);
      expect(key).toBe(EVENTS.POST_AUTH_LOGIN + '_' + DAY);
    });

  });

});
