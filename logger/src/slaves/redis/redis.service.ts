import { Injectable, Logger } from '@nestjs/common';
import * as moment from 'moment';
import { PayloadRedis } from '_interfaces/payload.interface';
import { ISlave } from '../../interfaces/slave.interface';
import { 
  CTX, 
  DateIncrement,
  DateIncrementDef,
  DateIncrementKeys
}  from '../../constants';
import { ClientService } from './client/client.service';

@Injectable()
export class RedisService implements ISlave {
  constructor(private readonly client: ClientService) {
  }
  
  async process(event: string, payload: PayloadRedis): Promise<boolean> {
    const increments = this._parseDateIncrements(payload.dateIncrements);
    increments.forEach(dateKey => {
      const key: string = this._getRedisKey(event, dateKey);
      Logger.verbose('Incr ' + key, CTX.REDIS);
      this.client.async().incr(key);
    });
    return true;
  }
  
  _getRedisKey(prefix: string, dateKey: DateIncrement): string {
    const format: string = moment().format(DateIncrementKeys[dateKey]);
    return `${prefix}_${format}`;
  }
  
  _parseDateIncrements(list: string = 'ymd'): DateIncrement[] {
    const increments: DateIncrement[] = [];
    if (!list) {
      return [];
    }
    list.split('').forEach(inc => {
      if (DateIncrementDef.hasOwnProperty(inc)) {
        increments.push(DateIncrementDef[inc]);
      }
    });
    return increments;
  }
}
