import { Injectable, Logger } from '@nestjs/common';
import { RedisClient, RedisError } from 'redis';
import { promisify } from 'util';
import { AsyncBridge } from '_interfaces/async-bridge.interface';
import { CTX }  from '../../../constants';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class ClientService {
    private readonly redis: RedisClient;
    private readonly asyncBridge: AsyncBridge;
    
    constructor(config: ConfigService) {
        if (!config.get('slaves.redis.databaseConnection', true)) {
            this.asyncBridge = {
                get: <T>(): Promise<T> => Promise.resolve(null),
                set: (): Promise<string> => Promise.resolve(null),
                incr: (): Promise<string> => Promise.resolve(null),
                decr: (): Promise<string> => Promise.resolve(null),
                hmset: (): Promise<string> => Promise.resolve(null),
                del: (): Promise<number> => Promise.resolve(null)
            };
            return;
        }
        this.redis = new RedisClient({
            host: process.env.REDIS_HOST || '127.0.0.1',
            port: parseInt(process.env.REDIS_PORT, 10) || 6379,
        });
        this.redis
        .on('error', this._onError)
        .on('connect', this._onConnect)
        .on('end', this._onEnd);
        this.asyncBridge = {
            get: promisify(this.redis.get).bind(this.redis),
            set: promisify(this.redis.set).bind(this.redis),
            incr: promisify(this.redis.incr).bind(this.redis),
            decr: promisify(this.redis.decr).bind(this.redis),
            hmset: promisify(this.redis.hmset).bind(this.redis),
            del: promisify(this.redis.del).bind(this.redis),
        };
    }
    
    public get(): RedisClient {
        return this.redis;
    }
    
    public async(): AsyncBridge {
        return this.asyncBridge;
    }
    
    private _onError(error: RedisError): void {
        Logger.error(error.message, error.stack, CTX.REDIS);
    }
    
    private _onConnect(): void {
        Logger.verbose('Connected to the database.', CTX.REDIS);
    }
    
    private _onEnd(): void {
        Logger.warn('Database connection closed.', CTX.REDIS);
    }
}
