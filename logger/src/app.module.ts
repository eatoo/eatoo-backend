import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RedisModule } from './slaves/redis/redis.module';
import { FileRequestModule } from './slaves/file-request/file-request.module';
import { ConfigModule } from './config/config.module';

@Module({
  imports: [RedisModule, FileRequestModule, ConfigModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
