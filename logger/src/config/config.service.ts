import { Injectable } from '@nestjs/common';
import * as config from 'config';

@Injectable()
export class ConfigService {
    get(key: string, defaultValue: any): any {
        return config.has(key) ? config.get(key) : defaultValue;
    }
}
