import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RedisModule } from './slaves/redis/redis.module';
import { FileRequestModule } from './slaves/file-request/file-request.module';
import { ClientService } from './slaves/redis/client/client.service';
import { ConfigService } from './config/config.service';
import { ConfigModule } from './config/config.module';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [RedisModule, FileRequestModule, ConfigModule],
      controllers: [AppController],
      providers: [
        AppService,
        ClientService,
        ConfigService
      ],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect('Hello World').toBe('Hello World');
    });
  });
});
