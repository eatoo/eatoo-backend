module.exports = {
  slaves: {
    redis: {
      databaseConnection: false,
    },
    fileRequest: {
      folderName: 'logs'
    },
  }
};
