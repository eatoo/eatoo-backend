module.exports = {
  slaves: {
    redis: {
      databaseConnection: true,
    },
    fileRequest: {
      folderName: 'logs'
    },
  }
};
