#!/usr/bin/env bash

helm upgrade --install			\
     -f eatoo-values.development.yaml	\
     --namespace development		\
     development			\
     ./eatoo
