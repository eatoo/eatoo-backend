#!/bin/bash

declare -a Hosts=(
    "104.209.151.145"
    "104.209.155.109"
    "52.188.17.206"
)

echo "Running [$@] on all hosts."

for host in ${Hosts[@]}; do
    echo "---------- Running on $host ..."
    ssh "eatoo@$host" $@
done
