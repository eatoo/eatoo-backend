#!/bin/bash

declare -a Hosts=(
    "104.40.142.56"
    "137.117.147.116"
    "51.144.126.116"
    "137.117.164.59"
    "51.136.57.161"
)

echo "Running [$@] on all hosts."

for host in ${Hosts[@]}; do
    echo "---------- Running on $host ..."
    ssh "eatoo-admin@$host" 'bash -s ' < $1
done
