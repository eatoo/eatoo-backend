# eatoo-backend

Version 1.9.14

## Politique de nommage des commits

Définit une politique de nommage pour les commits <br>
Projets ayant une politique de commits similaire :<br> 
NativeScript, Angular, Ionic, Vue.js, Nest.js, ElectronJS, Node.js, Nuxt.js, Prettier, Express.js, bootstrap-vue, react-bootstrap, Gin etc... 

```
<type>((<project>/)<scope>): <subject>

(<description>)
```

### Type

Définit le type du commit. <br>

- **docker** : Dockerfile, docker-compose
- **ci/cd** : Intégration continue / Déploiement continu
- **docs** : Documentation
- **feat** : Ajout d'une fonctionnalité
- **fix** : Correction de bugs
- **refactor** : Changement de code qui ne change rien au fonctionnement
- **style** : Changement de style du code (virgules, typos, eslint)
- **test** : Ajout/modification des tests

### Scope

Définit quelle partie du projet est affectée par le commit (optionnel).

- global
- api
  - api/auth
  - api/user
  - ...
- logger
  - logger/redis
  - logger/file
  - ...

### Subject

Contient une description succinte des changements 

- En utilisant l'impératif : "add" et non "adding" ou "added"
- Pas de majuscule au début
- Pas de "." à la fin

### Description

Permet de détailler plus en profondeur le commit. <br>
Mêmes règles que pour le Subject.

### Exemple

```
feat(api/user) : add route to update user

POST /user/update
```

```
docs(global) : add commits naming policy
```
