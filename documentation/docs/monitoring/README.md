---
sidebar: auto
---

# Monitoring

::: warning
@TODO : split docker-compose.yml for monitoring / app
:::

## Fonctionnement

Un système de monitoring est mis en place, permettant de surveiller et monitorer le serveur Azure ainsi que ses conteneurs Docker.

::: tip Informations sur le serveur Azure

**URL** : `https://x2021eatoo636384406001.northeurope.cloudapp.azure.com`

**Adresse IP** : 137.135.130.43

**Connexion via SSH** :
```sh
ssh eatoo_admin@x2021eatoo636384406001.northeurope.cloudapp.azure.com -p 60331
```

:::

Le système de monitoring est constitué d'un ensemble de services docker-compose communicant ensemble.

Un dashboard Grafana est mis à disposition afin de pouvoir consulter les données récoltées et analysées.

::: tip Accès à Grafana

**URL** : `https://grafana.137.135.130.43.xip.io`

**Username** : `admin`

**Password** : `admin`

:::

`xip.io` est utilisé en tant que reverse proxy pour rediriger le trafic de cette URL vers un certain port du serveur Azure, et sert également de DNS pour la génération du certificat SSL avec Let's Encrypt.

## Technologies

Les technologies utilisées sont les suivantes :

- [Grafana](/monitoring/#grafana)
- [NodeExporter](/monitoring/#nodeexporter)
- [Prometheus](/monitoring/#prometheus)
- [CAdvisor](/monitoring/#cadvisor)
- [PushGateway](/monitoring/#pushgateway)
- [AlertManager](/monitoring/#alertmanager)

----

![Archictecture](./monitoring.png)

### Grafana

*Utilisé par : `Paypal`, `Ebay`, `TED`, `Intel`, `Vimeo`, `Booking` ...*

Grafana permet d'effectuer des requêtes, de visualiser, d'alerter et de comprendre des données, peu importe où elles sont stockées. Grafana supporte plus de 30 sources de données, tels que `Graphite`, `InfluxDB`, `Prometheus`, `ElasticSearch`, `AWS CloudWatch`, `MySQL`, `DataDog` ...

### NodeExporter

NodeExporter est un exporteur Prometheus pour les métriques de matériel hardware et de système d'exploitation ayant des noyaux *NIX.

### Prometheus

Prometheus, un projet de la Cloud Native Computing Foundation, est un système de surveillance de systèmes et de services. Il collecte des métriques à partir de cibles configurées à des intervalles donnés, évalue les expressions de règle, affiche les résultats et peut déclencher des alertes si une condition est vérifiée.

### CAdvisor

cAdvisor (Container Advisor) permet aux utilisateurs de conteneurs de comprendre l'utilisation des ressources et les caractéristiques de performance de leurs conteneurs en cours d'exécution. Il s'agit d'un démon en cours d'exécution qui collecte, agrège, traite et exporte des informations sur l'exécution des conteneurs.

Plus précisément, pour chaque conteneur, il conserve les paramètres d'isolation des ressources, l'utilisation historique des ressources, les histogrammes de l'utilisation historique complète des ressources et les statistiques du réseau. Ces données sont exportées par conteneur et à l'échelle de la machine.

### PushGateway

Le Pushgateway Prometheus existe pour permettre aux travaux éphémères et batch d'exposer leurs métriques à Prometheus. Étant donné que ces types de travaux peuvent ne pas exister suffisamment longtemps pour être supprimés, ils peuvent à la place pousser leurs mesures vers un Pushgateway. Le Pushgateway expose ensuite ces mesures à Prometheus.

### AlertManager

Alertmanager gère les alertes envoyées par des applications clientes telles que le serveur Prometheus. Il prend en charge la déduplication, le regroupement et le routage vers les intégrations de récepteur correctes telles que la messagerie électronique, PagerDuty ou OpsGenie. Il prend également en charge le silence et l'inhibition des alertes.