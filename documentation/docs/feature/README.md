---
sidebar: auto
---

# Process d'Implémentation d'une route

## Feature

### Création de la feature

Créer la feature avec le nom souhaiter. Vous serez alors sur la branche du nom donner.

```
git flow feature start <Nom de la feature>
```

### Création de la route

L'architecture du dossier de la nouvelle route se présente ainsi :

::: tip Architecture du dossier
```
.
└── (Nom du dossier)
    ├── dto
    │   └── xx.dto.ts
    │── interfaces
    │   └── xx.interface.ts
    │── schemas
    │   └── xx.schema.ts
    │── xx.controller.spec.ts
    │── xx.controller.ts
    │── xx.module.ts
    │── xx.service.spec.ts
    └── xx.service.ts
```
:::

::: warning
Faire attention au nom des dossiers et fichiers (toujours en minuscule).
:::

- `folder/dto/xx.dto.ts` : Permet d'appliquer des critères de validation sur les données envoyé pour la route associée.
- `folder/interfaces/xx.interface.ts` : Permet de déterminer l'interface de la route associée.
- `folder/schemas/xx.schema.ts` : Permet de déterminer le schéma Mango de la route associée.
- `folder/xx.controller.spec.ts` : Permet d'effectuer des tests unitaires sur les fonctions du fichier associé.
- `folder/xx.controller.ts` : Fichier où les routes seront créees.
- `folder/xx.module.ts` : Fichier de création du module.
- `folder/xx.service.spec.ts` : Permet d'effectuer des tests unitaires sur les fonctions du fichier associé.
- `folder/xx.service.ts` : Fichier où les services des routes seront crées.


::: tip Création du module
**Path** : `(folder/xx.module.ts)`

```ts 
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { XXController } from './XX.controller';
import { XXService } from './XX.service';
import XXSchema from './schemas/XX.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'XX',
        schema: XXSchema,
      },
    ]),
  ],
  controllers: [XXController],
  providers: [XXService]
})
export class XXModule {
}
```
:::


::: tip Création du DTO
**Path** : `(folder/dto/xx.dto.ts)`

```ts 
import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class XX {

  @ApiModelProperty({minimum: 3})
  @IsNotEmpty()
  readonly name: string;

}
```
:::



::: tip Création de l'interface
**Path** : `(folder/interfaces/xx.interface.ts)`

```ts 
export interface XX {
    _id?: string;
    name: string;
    email: string;
    age: string;
  }
```
:::



::: tip Création d'un schema
**Path** : `(folder/schemas/xx.schema.ts)`

```ts 
import * as mongoose from 'mongoose';

const XX = new mongoose.Schema(
  {
    name: String,
    email: String,
    age: String,
  },
  {
    timestamps: true,
  },
);

XXSchema.pre('save', function(next) {
  return next();
});

XXSchema.methods.toJSON = function() {
  const obj = this.toObject();
  return obj;
};

export default XXSchema;
```
:::



::: tip Création du controller
**Path** : `(folder/xx.controller.ts)`

```ts 
import { Body, Controller, ApiUseTags, Post, , , , } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ApiResponse } from '@nestjs/swagger';
import { Model } from 'mongoose';
import { XXService } from './xx.service';
import { XXDto } from './dto/xx.dto';
import { XX } from './interfaces/xx.interface';

@ApiUseTags('XX')
@Controller('XX')
export class XXController {
  constructor(
    private readonly XXService: XXService,
    @InjectModel('XX') private readonly XXModel: Model<XX>,
    ) {
    }        
 
    /**
    * ! /XX/XX (url de la route)
    * @param dto 
    */
    @ApiResponse({ status: 201, description: '...' })
    @Post('create')
    async create(@Body() dto: XXDto): Promise<XXSuccess> {
    }
}
```
:::


::: tip Création du service
**Path** : `(folder/xx.service.ts)`

```ts 
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { XX } from './interfaces/XX.interface'

@Injectable()
export class XXService {
  constructor(
    @InjectModel('XX') private readonly XXModel: Model<XX>,
  ) {
  } 
}
```
:::


::: warning
Ne pas oublier d'ajouter notre module dans la liste des modules de l'application. (`src/app.module.ts`)
:::

### Tests e2e

L'architecture du dossier de test e2e se présente ainsi :

::: tip Architecture du dossier
```
.
└── test
    ├── (Nom du dossier)
    │   └── xx.spec.ts
    │── auth.e2e-spec.ts
    └── jest.e2e.json
```
:::

- `test/folder/xx.spec.ts` : Fichier où les tests de la route seront implémenter.
- `test/auth.e2e-spec.ts` : Fichier de paramètrage et appel des fichiers de tests.

::: warning
Faire attention au nom des dossiers et fichiers (toujours en minuscule).
:::

::: tip Ajouter son dossier de tests
**Path** : `(test/auth.e2e-spec.ts)`

```ts
import XXTester from './folder/XX.spec';

const urls = {
  XX: '/XX/XX',
};

describe('XX Tests(e2e)', () => XXTester(urls, XX));
```
:::

::: tip Création du fichier de tests
**Path** : `(test/folder/xx.spec.ts)`

```ts 
import { expect, assert } from 'chai';
import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../src/app.module';
import { ValidationPipe } from '@nestjs/common';

export default (urls, dishes) => {
  let app;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });
  
  /**
  * XX
  */
  it('XX', (done) => {
    request(app.getHttpServer())
    .post(urls.XX)   
    .send(XX)
    .end((err, res) => {
      expect(res.status).to.equal(201);
      done();
    });
  });

  afterAll(async () => {
    await app.close();
  });
};  
```
:::

### Tests unitaires

Chaque fichiers de tests unitaires se trouvent à la même racine que le fichier tester.

::: tip Création des .spec (les tests unitaires des fichiers associés)
**Path** : `(folder/xx.controller.spec.ts)`<br>
**Path** : `(folder/xx.service.spec.ts)`

```ts 
import { Test, TestingModule } from '@nestjs/testing';
import { XXController } from './XX.controller';
import { XXService } from './XX.service';

describe('XX Controller', () => {
  let controller: XXController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [],
      providers: []
    }).compile();

    controller = module.get<XXController>(XXController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

```
:::

### Fermeture de la feature

Ajouter vos fichiers modifier a l'aide de GIT.
```
git add --all
git commit -m "<Votre commit suivant la politique de commits>"
```

Fermer la feature pour permettre le merge avec la branche `develop`.
```
git flow feature finish <Nom de la feature>
```

## Release

### Création de la Release

Commencer par verifier le dernier `tag` ajouter. 
La release devra avoir un nombre suivant logique mise en place. 

```
git tag
```

Créer la release avec le nom souhaiter.

```
git flow release start <Nom de la release>
```

Quatre fichiers sont alors à modifier pour mettre à jour le versionnage :

`/README.md`

```md
# eatoo-backend

Version XXXX

## Politique de nommage des commits
```

`/api/config/default.ts`

```json
swagger: {
    title: 'Eatoo API Documentation',
    version: 'XXXX',
    path: 'doc',
},
```

`/api/package.json` and `/logger/package.json`
```json
{
 "name": "api",
  "version": "XXXX",
  "description": "",
}
```

### Fermeture de la Release

Ajouter vos fichiers modifier a l'aide de GIT.
```
git add --all
git commit -m "style(global) : update version number to <Nom de la release>"
```

Fermer la release pour permettre le merge avec la branche `develop`.
```
git flow release finish <Nom de la release>
```

Lors du merge, 3 fichiers de merge s'ouvriront.<br>
Pour le premier et troisième fichier, sauvergarder et quitter le fichier.<br>
Lors du deuxième, avant tout, éditer le avec le texte : `Release Version <Nom de la release>`.<br>

Une fois le merge effectuer, il ne suffit plus qu'a push sur les trois branches : <br> `master`, `develop` `<Nom de la release>`.
```
git push origin master develop <Nom de la release>
```
