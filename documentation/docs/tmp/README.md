# Authentification <Badge text="beta" type="warn"/>

[//]: <> ( -------------- ) 
[//]: <> (--- REGISTER ---) 
[//]: <> ( -------------- ) 

### 

<br>
<details>
<summary> <b>Register</b> </summary>

| POST   | 
| ------ |

Route : /auth/register

```json 
{
  "username": "string",
  "email": "string",
  "password": "string"
}
```

```bash 
RegisterDto
{
  username: string
            minimum: 3
  email:    string
  password: string
            minimum: 6
}
```

| Responses        | 
| ---------------- |

::: tip
**201** : Successful registration.
:::

::: warning
**400** : Bad request. Email or username has already been taken.
:::

::: danger
**500** : An error occurred on our side.
:::

</details>

[//]: <> ( --------------- ) 
[//]: <> (----- LOGIN -----) 
[//]: <> ( --------------- ) 

<br>
<details>
<summary> <b>Login</b> </summary>

| POST   | 
| ------ |

Route : /auth/login

```json 
{
  "username": "string",
  "email": "string",
  "password": "string"
}
```
```bash 
LoginDto
{
  username: string
            minimum: 3
  email:    string
  password: string
            minimum: 6
}
```

| Responses        | 
| ---------------- |

::: tip
**200** : Successful connection.
:::

::: warning
**400** : Bad request.
:::

::: danger
**401** : Authentication failed.
:::

</details>

[//]: <> ( ---------------- ) 
[//]: <> (------- ME -------) 
[//]: <> ( ---------------- ) 

<br>
<details>
<summary> <b>Get me</b> </summary>

| GET   | 
| ------ |

Route : /auth/me

```json 
{
  No Parameters
}
```

```bash 
{
 No DTO
}
```

| Responses        | 
| ---------------- |

::: tip
**200** : Returns the user corresponding to the provided JWT.
:::

::: warning
**401** : Unauthorized.
:::

::: danger
**403** : Forbidden.
:::

</details>


# Profil <Badge text="beta" type="warn"/>


[//]: <> ( ---------------- ) 
[//]: <> (------- ME -------) 
[//]: <> ( ---------------- ) 


<br>
<details>
<summary> <b>Update me</b> </summary>

| PATCH   | 
| ------- |

Route : /auth/me

```json 
{
  "email": "string"
}
```

```bash 
UpdateDto
{
email:	string
}
```

| Response         | 
| ---------------- |

::: tip
**200** : Returns the updated user.
:::

</details>
