---
sidebar: auto
---

# EATOO

## Présentation

EATOO se divise en 3 grandes parties :

- [API](/#api)
- [Logger](/#logger)
- [Documentation](/#documentation)

## API

### Informations

...

### Technologies

Nest (NestJS) est un framework pour la création d'applications efficaces et évolutives côté serveur Node.js.<br>
Il utilise le JavaScript progressif, est construit avec et supporte entièrement le TypeScript (tout en permettant aux développeurs de coder en JavaScript pur) et combine des éléments de OOP (Object Oriented Programming), FP (Functional Programming), et FRP (Functional Reactive Programming).

![NestJs](./pictures/nest-logo.png)

MongoDB Atlas est le service mondial de base de données en nuage pour les applications modernes. <br>
Déployez MongoDB entièrement géré à travers AWS, Azure ou GCP. <br>
Une automatisation de pointe et des pratiques éprouvées garantissent la disponibilité, l'évolutivité et la conformité aux normes les plus exigeantes en matière de sécurité et de confidentialité des données. <br>
Utilisez le robuste écosystème de pilotes, d'intégrations et d'outils de MongoDB pour construire plus rapidement et passer moins de temps à gérer votre base de données.

![MongoDB](./pictures/mongoDB-logo.jpeg)

Jest est un framework de test JavaScript qui met l'accent sur la simplicité. <br>
Il fonctionne avec des projets utilisant : Babel, TypeScript, Node, React, Angular, Vue et plus encore !

![Jest](./pictures/jest-logo.png)

Mocha est un framework de test JavaScript riche en fonctionnalités qui fonctionne sur Node.js et dans le navigateur, rendant les tests asynchrones simples et amusants. <br>
Les tests Mocha s'exécutent en série, permettant un reporting flexible et précis, tout en mappant les exceptions non détectées aux bons cas de test.

Chai est une bibliothèque d'assertions qui est souvent utilisée avec Mocha. <br> 
Elle fournit des fonctions et des méthodes qui vous aident à comparer la sortie d'un certain test avec sa valeur attendue. <br>
Chai fournit une syntaxe propre qui se lit presque comme de l'anglais !

![MochaChai](./pictures/mocha-chai-logo.jpg)

Swagger est un framework logiciel open-source soutenu par un large écosystème d'outils qui aide les développeurs à concevoir, construire, documenter et consommer des RESTful web services.

![Swagger](./pictures/swagger-logo.png)

## Logger

### Informations

...

### Technologies

Redis (Remote Dictionary Server) est un projet de structure de données en mémoire mettant en œuvre une base de données distribuée de valeurs clés en mémoire avec une durabilité optionnelle. <br>
Redis supporte différents types de structures de données abstraites, telles que des chaînes de caractères, des listes, des cartes, des ensembles, des ensembles triés, des HyperLogLogs, des bitmaps, des flux et des index spatiaux. 

![Redis](./pictures/redis-logo.png)

## Documentation

### Informations

...

### Technologies

VuePress est un générateur de site statique basé sur le framework JavaScript de Vue.<br>
VuePress a la flexibilité de construire n'importe quel site statique, mais il excelle particulièrement dans le travail avec la documentation.


![VuePress](./pictures/vuePress-logo.jpeg)



