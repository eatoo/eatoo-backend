module.exports = {
  title: 'Eatoo API Documentation',
  description: 'Just playing around',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Process d\'Implémentation', link: '/feature/' },
      { text: 'Monitoring', link: '/monitoring/' },
      { text: 'Nommage des commits', link: '/commits/' },
      { text: 'Architecture', link: '/architecture/' },
    ],
  }
}